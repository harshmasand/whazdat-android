package com.whazdat.application;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.whazdat.R;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.interfaces.APIEndpointInterface;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author by Harsh Masand on 6/6/18.
 */

public class WhazdatApplication extends MultiDexApplication {

    private static final String TAG = WhazdatApplication.class.getSimpleName();
    @SuppressLint("StaticFieldLeak")
    static Context context;
    static WhazdatApplication application;
    private static APIEndpointInterface apiEndpointInterface;
    private SharedPreferences sharedPreferences;
    public static final String IMAGES_FOLDER = "Images";
    public static final String SUPPORT_IMAGES_FOLDER = "Support Images";
    private static File EXTERNAL_IMAGE_FILES_DIR;
    private static File INTERNAL_IMAGES_DIR;
    public static Context getNewApplicationContext() {
        return context;
    }

    public static WhazdatApplication getApplication() {
        return application;
    }

    public static File getExternalImageFilesDir() {
        return EXTERNAL_IMAGE_FILES_DIR;
    }

    public static File getInternalImagesDir() {
        return INTERNAL_IMAGES_DIR;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        context = getApplicationContext();

        EXTERNAL_IMAGE_FILES_DIR = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        if (EXTERNAL_IMAGE_FILES_DIR == null) {
            INTERNAL_IMAGES_DIR = new File(getDir(IMAGES_FOLDER, MODE_PRIVATE).getPath() + File.separator + SUPPORT_IMAGES_FOLDER);
            if (!INTERNAL_IMAGES_DIR.exists()) {
                INTERNAL_IMAGES_DIR.mkdirs();
            }
        }
        try {
            WhazdatSharedPrefs.initialize(this);
        } catch (Exception e) {
            Log.e(TAG, "" + e.getMessage(), e);
        }
        sharedPreferences = WhazdatSharedPrefs.getInstance();
    }

    public static APIEndpointInterface getApiEndpointInterface() {
        if (apiEndpointInterface == null) {
            apiEndpointInterface = createAPIEndpoint();
        }
        return apiEndpointInterface;
    }

    private static APIEndpointInterface createAPIEndpoint() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                                            .connectTimeout(30, TimeUnit.SECONDS)
                                            .readTimeout(30, TimeUnit.SECONDS)
                                            .writeTimeout(30, TimeUnit.SECONDS)
                                            .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(application.getResources().getString(R.string.base_url))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit.create(APIEndpointInterface.class);
    }

    public double getLatitude(){
        //TODO : have to change later
        //return 45.4116698429;
        return Double.valueOf(sharedPreferences.getString(SharedPrefsConstants.LATITUDE,"45.4116698429"));
    }

    public double getLongitude(){
        //TODO : have to change later
        //return -71.9013671875;
        return Double.valueOf(sharedPreferences.getString(SharedPrefsConstants.LONGITUDE,"-71.9013671875"));
    }

    public long getLatitudeInLong(){
        //TODO : have to change later
        //return 45.4116698429;
        return Long.valueOf(sharedPreferences.getString(SharedPrefsConstants.LATITUDE,"45.4116698429"));
    }

    public long getLongitudeInLong(){
        //TODO : have to change later
        //return -71.9013671875;
        return Long.valueOf(sharedPreferences.getString(SharedPrefsConstants.LONGITUDE,"-71.9013671875"));
    }


}
