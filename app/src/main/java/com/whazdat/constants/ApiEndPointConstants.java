package com.whazdat.constants;

/**
 * @author by Harsh Masand on 8/6/18.
 */

public interface ApiEndPointConstants {

    String LOGIN = "login_to_app";
    String REGISTER = "register_to_app";
    String PROFILE = "show_user_profile";
    String SUBSCRIPTION = "get_subscription_list";
    String BOOKMARK = "set_book_mark_place";
    String PLACE_DETAILS = "show_place_details";
    String PLACE_LIST = "show_nearby_list";
    String LOCATION_SEARCH = "show_place_by_name";
    String BOOKMARKED_PLACES = "get_book_mark_list";
    String INIT = "show_place_in_range";
    String GET_USER_AUTH = "get_user_auth";

}
