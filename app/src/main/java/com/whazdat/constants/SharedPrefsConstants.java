package com.whazdat.constants;

/**
 * @author by Harsh Masand on 7/6/18.
 */

public interface SharedPrefsConstants {

    String LATITUDE = "latitude";
    String LONGITUDE = "longitude";
    String USER_EMAIL_ADDRESS = "userEmailAddress";
    String USER_NAME = "userName";
    String USER_IMAGE_URL = "userImageUrl";
    String ID = "id";
    String LOGGED_IN = "LoggedIN";
    String ONBOARDING_DONE = "onboardingDone";
    String SHARED_PREFS_CLEARED_ONCE = "sharedPrefsClearedOnce";

    String ADD_PLACE_LATITUDE = "placeLatitude";
    String ADD_PLACE_LONGITUDE = "placeLongitude";
    String SECRET_HASH_KEY = "secretHashKey";
    String ORDER_ID = "orderID";
    String AUDIO_SIZE = "audioSize";
    String VIDEO_SIZE = "videoSize";
    String IMAGE_SIZE = "imageSize";
}
