package com.whazdat.constants;

/**
 * @author by Harsh Masand on 28/4/18.
 */

public interface WhazdatIntentConstants {

    String PLACE_DATA = "placeData";
    String MEDIA_TYPE = "mediaType";
    String MEDIA_URL = "mediaURL";
    String ORDER_ID  = "orderID";
    String IMAGE_URL = "imageURL";
    String FROM_DATA_LOADING = "fromDataLoading";
}
