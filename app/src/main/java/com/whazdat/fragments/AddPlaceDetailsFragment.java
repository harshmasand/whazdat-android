package com.whazdat.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.Toast;

import com.whazdat.R;
import com.whazdat.activity.AddPlaceActivity;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.model.StatusResponse;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.InternetStatusHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPlaceDetailsFragment extends Fragment{

    Context context;
    TextInputLayout placeNameTil,dobTil,aboutTil,dodTil;
    AutoCompleteTextView placeName,btnDOB,btnDemise,about, latitude,longitude;
    SharedPreferences preferences;
    InternetStatusHelper internetStatusHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = WhazdatSharedPrefs.getInstance();
        internetStatusHelper = new InternetStatusHelper(context);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_add_place_details, container, false);
        placeName = fragmentView.findViewById(R.id.place_name);
        btnDOB = fragmentView.findViewById(R.id.bt_exp_date);
        btnDemise = fragmentView.findViewById(R.id.bt_demise_date);
        about = fragmentView.findViewById(R.id.about_text);
        latitude = fragmentView.findViewById(R.id.latitude_text);
        longitude = fragmentView.findViewById(R.id.longitude_text);
        placeNameTil = fragmentView.findViewById(R.id.place_name_til);
        dobTil = fragmentView.findViewById(R.id.dob_til);
        aboutTil = fragmentView.findViewById(R.id.about_til);
        dodTil = fragmentView.findViewById(R.id.dod_til);
        btnDemise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogDatePicker(btnDemise);
            }
        });
        btnDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogDatePicker(btnDOB);
            }
        });
        return fragmentView;
    }



    private void dialogDatePicker(final View v) {
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog  StartTime = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                final Calendar newDate = Calendar.getInstance();
                newDate.set(Calendar.YEAR, year);
                newDate.set(Calendar.MONTH, monthOfYear);
                newDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                long date = newDate.getTimeInMillis();

                ((AutoCompleteTextView) v).setText(getFormattedDateShort(date));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        StartTime.show();
    }

    public static String getFormattedDateShort(Long dateTime) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat newFormat = new SimpleDateFormat("yyyy/MM/dd");
        return newFormat.format(new Date(dateTime));
    }

    public boolean validate(){
        boolean isValidate = true;
        if(placeName.getText().toString().trim().equalsIgnoreCase("")){
            placeNameTil.setError("Please add place name");
            isValidate = false;
        }else {
            placeNameTil.setError(null);
        }

        if(btnDOB.getText().toString().equalsIgnoreCase("date of birth")){
            dobTil.setError("Please a date of birth");
            isValidate = false;
        }else{
            dobTil.setError(null);
        }

        if(btnDemise.getText().toString().equalsIgnoreCase("date of demise")){
            dodTil.setError("Please a date of demise");
            isValidate = false;
        }else{
            dodTil.setError(null);
        }

        if(about.getText().toString().trim().equalsIgnoreCase("")){
            aboutTil.setError("Please add the description about the place");
            isValidate = false;
        }else{
            aboutTil.setError(null);
        }

        return isValidate;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            if(latitude != null && longitude != null && preferences != null){
                latitude.setText(preferences.getString(SharedPrefsConstants.ADD_PLACE_LATITUDE, ""));
                longitude.setText(preferences.getString(SharedPrefsConstants.ADD_PLACE_LONGITUDE,""));
            }
        }
    }

    public void addPlace(){
        if(internetStatusHelper.isConnected()){
            Call<StatusResponse> call = WhazdatApplication.getApiEndpointInterface().addPlace(placeName.getText().toString(), latitude.getText().toString(), longitude.getText().toString(), about.getText().toString(), btnDOB.getText().toString(), btnDemise.getText().toString(), 1, preferences.getInt(SharedPrefsConstants.ORDER_ID, 0), preferences.getString(SharedPrefsConstants.SECRET_HASH_KEY, ""));
            call.enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                    if (response.body() != null) {
                        if (response.body().isSuccess()) {
                            //Next Screen
                            if (getActivity() != null) {
                                ((AddPlaceActivity) getActivity()).nextScreen();
                            }
                        } else {
                            Toast.makeText(getContext(), "Something went wrong!!, please try again later", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Something went wrong!!, please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {
                    if (t != null && t.getMessage() != null) {
                        Toast.makeText(getContext(), "Something went wrong!!, please try again later", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else{
            internetStatusHelper.showNoInternetToast(context);
        }
    }
}
