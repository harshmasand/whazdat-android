package com.whazdat.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.whazdat.R;
import com.whazdat.adapter.MySubscriptionListAdapter;
import com.whazdat.model.MySubscriptionData;
import com.whazdat.util.ErrorView;
import com.whazdat.util.InternetStatusHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by Harsh Masand on 16/3/18.
 */

public class TestFragment extends Fragment {
    ProgressBar loader;
    ErrorView errorView;
    RecyclerView recyclerView;
    InternetStatusHelper internetStatusHelper;
    List<MySubscriptionData> list;
    MySubscriptionListAdapter mySubscriptionListAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        internetStatusHelper = new InternetStatusHelper(getContext());
        list = new ArrayList<>();
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_subscriptions, container, false);
        recyclerView = view.findViewById(R.id.recycle_view_list);
        errorView = view.findViewById(R.id.error_layout_container);
        loader = view.findViewById(R.id.loader);
        mySubscriptionListAdapter = new MySubscriptionListAdapter(getContext(),list,R.layout.list_item_subscription);
        recyclerView.setNestedScrollingEnabled(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mySubscriptionListAdapter);
        fetchMySubscriptions();
        return view;
    }

    private void fetchMySubscriptions() {
        if(internetStatusHelper.isConnected()){
            loader.setVisibility(View.GONE);
            errorView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

        }else{
            internetStatusHelper.showNoInternetToast(getContext());
        }
    }
}
