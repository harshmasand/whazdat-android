package com.whazdat.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.whazdat.R;
import com.whazdat.activity.AddPlaceActivity;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.ErrorView;

import static com.whazdat.fragments.MySubscriptionFragment.TAG;

public class LatLngFragment extends Fragment implements OnMapReadyCallback {

    MapView mapView;
    ErrorView errorView;
    GoogleMap mMap;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = WhazdatSharedPrefs.getInstance();
        editor = WhazdatSharedPrefs.getEditor();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_lat_lng, container, false);
        mapView = fragmentView.findViewById(R.id.map_view);
        errorView = fragmentView.findViewById(R.id.error_view);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        return fragmentView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng latLng = mMap.getCameraPosition().target;
                double lat =  latLng.latitude;
                double lng =  latLng.longitude;
                editor.putString(SharedPrefsConstants.ADD_PLACE_LATITUDE, String.valueOf(lat));
                editor.putString(SharedPrefsConstants.ADD_PLACE_LONGITUDE, String.valueOf(lng)).apply();
                Log.e("LatLONG", lat+ " --- "+lng);
                mMap.getUiSettings().setAllGesturesEnabled(true);
                if (getActivity() != null)
                    ((AddPlaceActivity) getActivity()).enableButton();
            }
        });

        LatLng latLng = new LatLng(Double.valueOf(preferences.getString(SharedPrefsConstants.ADD_PLACE_LATITUDE, String.valueOf(WhazdatApplication.getApplication().getLatitude()))),Double.valueOf(preferences.getString(SharedPrefsConstants.ADD_PLACE_LONGITUDE, String.valueOf(WhazdatApplication.getApplication().getLongitude()))));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));

        //Plot the current location in the map
        //Allow User to drag the marker
        //When user have stopped the Dragging , get the coordinates and Store
    }



    @Override
    public void onLowMemory() {
        super.onLowMemory();
        try {
            mapView.onLowMemory();
        } catch (Exception e) {
            Log.e(TAG, " " + e.getMessage(), e);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mapView.onResume();

        } catch (Exception e) {
            Log.e(TAG, " " + e.getMessage(), e);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mapView.onDestroy();

        } catch (Exception e) {
            Log.e(TAG, " " + e.getMessage(), e);
        }
    }
}
