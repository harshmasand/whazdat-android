package com.whazdat.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.whazdat.R;
import com.whazdat.activity.MyPlacesInnerActivity;
import com.whazdat.adapter.MyPlacesListAdapter;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.ApiEndPointConstants;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.constants.WhazdatIntentConstants;
import com.whazdat.listener.RecyclerItemListener;
import com.whazdat.model.BookmarkedPlacesResponse;
import com.whazdat.model.PlacesData;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.ErrorView;
import com.whazdat.util.InternetStatusHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.whazdat.fragments.MySubscriptionFragment.TAG;

/**
 * @author by Harsh Masand on 13/3/18.
 */

public class MyPlacesFragment extends Fragment {
    ProgressBar loader;
    ErrorView errorView;
    RecyclerView recyclerView;
    InternetStatusHelper internetStatusHelper;
    List<PlacesData> myPlacesDataList;
    MyPlacesListAdapter myPlacesListAdapter;
    SharedPreferences sharedPreferences;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        internetStatusHelper = new InternetStatusHelper(getActivity());
        sharedPreferences = WhazdatSharedPrefs.getInstance();
        myPlacesDataList = new ArrayList<>();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_places, container, false);
        recyclerView = view.findViewById(R.id.recycle_view_list);
        errorView = view.findViewById(R.id.error_layout_container);
        loader = view.findViewById(R.id.loader);
        GridLayoutManager mLayoutManager = new GridLayoutManager(getContext(),2);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(true);
        if(errorView.getVisibility() == View.VISIBLE){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                errorView.setNestedScrollingEnabled(true);
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                errorView.setNestedScrollingEnabled(false);
            }
        }
        myPlacesListAdapter = new MyPlacesListAdapter(getContext(),R.layout.list_item_my_places,myPlacesDataList);
        recyclerView.setAdapter(myPlacesListAdapter);
        errorView.setOnRetryButtonClickListener(new ErrorView.OnRetryButtonClickListener() {
            @Override
            public void onClick() {
                //Retry Button On Click
                fetchMyPlaces();
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerItemListener(getContext(), recyclerView, new RecyclerItemListener.RecyclerTouchListener() {
            @Override
            public void onClickItem(View v, int position) {
                PlacesData placesDataItem = myPlacesDataList.get(position);
                Intent intent = new Intent(getActivity(), MyPlacesInnerActivity.class);
                intent.putExtra(WhazdatIntentConstants.ORDER_ID,placesDataItem.getOrderID());
                startActivity(intent);
            }

            @Override
            public void onLongClickItem(View v, int position) {
                //Do Nothing
            }
        }));
        fetchMyPlaces();
        return view;
    }

    private void fetchMyPlaces() {
        if(internetStatusHelper.isConnected()){
            showLoaderView();
            Call<BookmarkedPlacesResponse> call = WhazdatApplication.getApiEndpointInterface().getBookmarkedPlaces(ApiEndPointConstants.BOOKMARKED_PLACES,sharedPreferences.getInt(SharedPrefsConstants.ID,0));
            call.enqueue(new Callback<BookmarkedPlacesResponse>() {
                @Override
                public void onResponse(Call<BookmarkedPlacesResponse> call, Response<BookmarkedPlacesResponse> response) {
                    if(response.body() != null ){
                        BookmarkedPlacesResponse apiResponse = response.body();
                        if(apiResponse.getData() != null){
                            ArrayList<PlacesData> placesDataList = apiResponse.getData();
                            if(placesDataList != null && !placesDataList.isEmpty()) {
                                myPlacesDataList.clear();
                                myPlacesDataList.addAll(placesDataList);
                                myPlacesListAdapter.notifyDataSetChanged();
                                showDataView();
                            }
                        }else{
                            showNoDataAvailableView();
                        }
                    }else{
                        //TODO : change Later to showErrorView();
                        showNoDataAvailableView();
                    }
                }

                @Override
                public void onFailure(Call<BookmarkedPlacesResponse> call, Throwable t) {
                    if(t !=null && t.getMessage() !=null){
                        Log.e(TAG,t.getMessage()+"");
                    }
                    showErrorView();
                }
            });
        }else{
            showNoInternetView();
        }
    }

    private void showNoDataAvailableView() {
        showDataInfo(getString(R.string.no_results_text), getString(R.string.no_data_message),false);
    }

    public void showLoaderView(){
        loader.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }

    public void showDataView(){
        loader.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    public void showErrorView(){
        showDataInfo(getString(R.string.oops_text), getString(R.string.something_went_wrong_text),true);
    }

    public void showNoInternetView(){
        showDataInfo(getString(R.string.oops_text),getString(R.string.you_are_offline_text),true);
    }

    private void showDataInfo(String title, String subtitle,boolean showRetryButton) {
        errorView.changeTitleText(title);
        errorView.changeSubtitleText(subtitle);
        if (showRetryButton) {
            errorView.showButton();
        }else {
            errorView.hideButton();
        }
        loader.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }

    public void refreshData(){
        fetchMyPlaces();
    }
}
