package com.whazdat.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.whazdat.R;
import com.whazdat.activity.HomeActivity;
import com.whazdat.activity.MyPlacesInnerActivity;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.ApiEndPointConstants;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.constants.WhazdatIntentConstants;
import com.whazdat.model.LatLngData;
import com.whazdat.model.LatLongListResponse;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.ErrorView;
import com.whazdat.util.InternetStatusHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author by Harsh Masand on 29/3/18.
 */

public class NearByFragment extends Fragment {

    private static final String TAG = NearByFragment.class.getSimpleName();
    MapView mapView;
    GoogleMap mMap;
    ArrayList<LatLngData> latLngArrayList;
    InternetStatusHelper internetStatusHelper;
    ErrorView errorView;
    ProgressBar loader;
    SharedPreferences sharedPreferences;
    Bundle savedInstanceState;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        internetStatusHelper = new InternetStatusHelper(getActivity());
        sharedPreferences = WhazdatSharedPrefs.getInstance();
        latLngArrayList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_near_by, container, false);
        mapView = view.findViewById(R.id.map_view);
        mapView.onCreate(savedInstanceState);
        errorView = view.findViewById(R.id.error_view);
        errorView.setOnRetryButtonClickListener(new ErrorView.OnRetryButtonClickListener() {
            @Override
            public void onClick() {
                fetchLatLongList();
            }
        });
        loader = view.findViewById(R.id.loader);
        fetchLatLongList();
        return view;
    }

    public void fetchLatLongList() {
        if(internetStatusHelper.isConnected()){
            showLoaderView();
            Call<LatLongListResponse> call = WhazdatApplication.getApiEndpointInterface().getLatLongList(ApiEndPointConstants.PLACE_LIST,sharedPreferences.getInt(SharedPrefsConstants.ID,0),WhazdatApplication.getApplication().getLatitude(),WhazdatApplication.getApplication().getLongitude());
            call.enqueue(new Callback<LatLongListResponse>() {
                @Override
                public void onResponse(Call<LatLongListResponse> call, Response<LatLongListResponse> response) {
                    if(response.body() != null){
                        LatLongListResponse apiResponse = response.body();
                        if(apiResponse.isSuccess()){
                            ArrayList<LatLngData> latLonList = apiResponse.getData();
                            if(latLonList != null && !latLonList.isEmpty()) {
                                latLngArrayList.clear();
                                latLngArrayList.addAll(latLonList);
                                LatLngData latLngData = new LatLngData();
                                latLngData.setLatitude(WhazdatApplication.getApplication().getLatitude());
                                latLngData.setLongitude(WhazdatApplication.getApplication().getLongitude());
                                latLngData.setOrderID(0);
                                latLngData.setPlaceName("Current Location");
                                latLngArrayList.add(latLngData);
                                setMapAndPlotLatLon(latLngArrayList);
                            }else{
                                showNoDataAvailableView();
                            }
                        }else{
                            showNoDataAvailableView();
                        }
                    }else{
                        showErrorView();
                    }
                }

                @Override
                public void onFailure(Call<LatLongListResponse> call, Throwable t) {
                    if(t != null && t.getMessage() != null){
                        Log.e(TAG,t.getMessage()+"");
                    }
                    showErrorView();
                }
            });
        }else{
            showNoInternetView();
        }
    }

    private void setMapAndPlotLatLon(final ArrayList<LatLngData> latLngArrayList) {
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
               mMap = googleMap;
               for(int i = 0; i < latLngArrayList.size() ; i++)
                {
                    Marker marker;
                    Geocoder geocoder = new Geocoder(getContext());
                    Address address = null;
                    List<Address> addressesList;
                    LatLng latLngLocation = new LatLng(latLngArrayList.get(i).getLatitude(),latLngArrayList.get(i).getLongitude());
                    try {
                        addressesList = geocoder.getFromLocation(latLngArrayList.get(i).getLatitude(),latLngArrayList.get(i).getLongitude(),1);
                        if(addressesList != null && !addressesList.isEmpty()){
                            address = addressesList.get(0);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if(i != latLngArrayList.size()-1) {
                        marker = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.address_locations)).position(latLngLocation));
                        marker.setTag(latLngArrayList.get(i).getOrderID());
                        if(address != null){
                        marker.setTitle(latLngArrayList.get(i).getPlaceName());
                        }


                    }else{
                        marker = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.currentlocation)).position(latLngLocation));
                        marker.setTag(latLngArrayList.get(i).getOrderID());
                        if(address != null){
                            marker.setTitle(latLngArrayList.get(i).getPlaceName());
                        }
                        latLngLocation = new LatLng(WhazdatApplication.getApplication().getLatitude(),WhazdatApplication.getApplication().getLongitude());
                        CircleOptions circleOptionsFirstSecond = new CircleOptions()
                                .center(latLngLocation)
                                .radius(200)
                                .fillColor(ContextCompat.getColor(getContext(),R.color.circular_shade_light))
                                .strokeColor(Color.TRANSPARENT);
                        mMap.addCircle(circleOptionsFirstSecond);
                    }
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngLocation,10));
                }
                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        marker.showInfoWindow();
                        // Return false to indicate that we have not consumed the event and that we wish
                        // for the default behavior to occur (which is for the camera to move such that the
                        // marker is centered and for the marker's info window to open, if it has one).
                        return false;
                    }
                });
               mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                   @Override
                   public void onInfoWindowClick(Marker marker) {
                       Integer  orderID = (Integer) marker.getTag();
                       if(orderID != null){
                           if(orderID != 0) {
                               Intent openPlaceDetailsIntent = new Intent(getActivity(), MyPlacesInnerActivity.class);
                               openPlaceDetailsIntent.putExtra(WhazdatIntentConstants.ORDER_ID, orderID);
                               startActivity(openPlaceDetailsIntent);
                           }
                       }else{
                           Toast.makeText(getContext(),"Something Went wrong!! Please try again",Toast.LENGTH_SHORT).show();
                       }
                   }
               });
            }
        });
        showDataView();
    }

    public void showLoaderView(){
        loader.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
        mapView.setVisibility(View.GONE);
    }

    public void showDataView(){
        loader.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        mapView.setVisibility(View.VISIBLE);
    }

    public void showNoDataAvailableView(){
        showDataInfo(getString(R.string.no_results_text), getString(R.string.no_data_message),false);
    }

    public void showErrorView(){
        showDataInfo(getString(R.string.oops_text), getString(R.string.something_went_wrong_text),true);
    }

    public void showNoInternetView(){
        showDataInfo(getString(R.string.oops_text),getString(R.string.you_are_offline_text),true);
    }

    private void showDataInfo(String message, String title , boolean showRetryButton) {
        errorView.changeTitleText(message);
        errorView.changeSubtitleText(title);
        if (showRetryButton) {
            errorView.showButton();
        }else {
            errorView.hideButton();
        }
        loader.setVisibility(View.GONE);
        mapView.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        try {
            mapView.onLowMemory();
        } catch (Exception e) {
            Log.e(TAG, " " + e.getMessage(), e);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mapView.onResume();
        } catch (Exception e) {
            Log.e(TAG, " " + e.getMessage(), e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            mapView.onPause();

        } catch (Exception e) {
            Log.e(TAG, " " + e.getMessage(), e);
        }
    }



    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).changeToolbarTitleAndSubtitle("Near By ", latLngArrayList.size()+" Places Around");
        }
    }
}
