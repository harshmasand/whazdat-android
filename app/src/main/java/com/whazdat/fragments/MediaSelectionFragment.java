package com.whazdat.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.whazdat.R;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.model.StatusResponse;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.InternetStatusHelper;
import com.whazdat.util.MediaManager;

import java.io.File;
import java.io.FileInputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class MediaSelectionFragment extends Fragment {

    public static final int PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 2;
    public static final int SELECT_IMAGE = 100;
    public static final int SELECT_AUDIO = 101;
    public static final int SELECT_VIDEO = 102;
    private static final String AWS_KEY = "AKIAJ5RFAOBCEPPKZNFQ";
    private static final String AWS_SECRET = "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";
    private static final String AWS_BUCKET = "whazdat";
    Context context;
    LinearLayout uploadImage, uploadVideo, uploadAudio;
    MediaManager mediaManager;
    SharedPreferences preferences;
    long fileSizeLimit = 1024 * 1024;
    ProgressDialog progressDialog;
    int audioSize, videoSize, imageSize;
    String type;
    InternetStatusHelper internetStatusHelper;
    String mediaURL;
    boolean isImageAvailable, isAudioAvailable, isVideoAvailable, isImageUploaded, isAudioUploaded, isVideoUploaded;
    ImageView audioUploaded, videoUploaded, imageUploaded;

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getRealPathFromURI_API19(Context context, Uri uri) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);
        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];
        String[] column = {MediaStore.Audio.Media.DATA};
        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getRealPathFromURI_API19Video(Context context, Uri uri) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);
        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];
        String[] column = {MediaStore.Video.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mediaManager = new MediaManager(getActivity(), this);
        preferences = WhazdatSharedPrefs.getInstance();
        internetStatusHelper = new InternetStatusHelper(getContext());
        audioSize = preferences.getInt(SharedPrefsConstants.AUDIO_SIZE, 0);
        imageSize = preferences.getInt(SharedPrefsConstants.IMAGE_SIZE, 0);
        videoSize = preferences.getInt(SharedPrefsConstants.VIDEO_SIZE, 0);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_add_media, container, false);
        uploadImage = fragmentView.findViewById(R.id.upload_image_btn);
        uploadAudio = fragmentView.findViewById(R.id.upload_audio_btn);
        uploadVideo = fragmentView.findViewById(R.id.upload_video_btn);
        audioUploaded = fragmentView.findViewById(R.id.audio_uploaded_image);
        videoUploaded = fragmentView.findViewById(R.id.video_uploaded_image);
        imageUploaded = fragmentView.findViewById(R.id.image_uploaded_image);
        checkUploadedMediaAndSet();
        if (audioSize > 0) {
            isAudioAvailable = true;
            uploadAudio.setVisibility(View.VISIBLE);
        } else {
            isAudioAvailable = false;
            uploadAudio.setVisibility(View.GONE);
        }

        if (imageSize > 0) {
            isImageAvailable = true;
            uploadImage.setVisibility(View.VISIBLE);
        } else {
            isImageAvailable = false;
            uploadImage.setVisibility(View.GONE);
        }

        if (videoSize > 0) {
            isVideoAvailable = true;
            uploadVideo.setVisibility(View.VISIBLE);
        } else {
            isVideoAvailable = false;
            uploadVideo.setVisibility(View.GONE);
        }
        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission()) {
                    type = "image";
                    openMediaGalleryByType(type);
                }
            }
        });

        uploadVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission()) {
                    type = "video";
                    openMediaGalleryByType(type);
                }
            }
        });

        uploadAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission()) {
                    type = "audio";
                    openMediaGalleryByType(type);
                }
            }
        });

        return fragmentView;
    }

    private boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if ((ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle(getString(R.string.alert));
                    alertBuilder.setMessage(getString(R.string.external_storage_camera_permission));
                    alertBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public void openMediaGalleryByType(String type) {
        Intent intent = new Intent();
        switch (type) {
            case "image":
                // Show only images, no videos or anything else
                mediaManager.selectImageFromGallery();
                break;
            case "audio":

                // Show only audio
                intent.setType("audio/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, SELECT_AUDIO);
                break;
            case "video":

                // Show only videos
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, SELECT_VIDEO);
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == MediaManager.SELECT_PICTURE && resultCode == RESULT_OK) {

                final Uri imageUri = mediaManager.onActivityResult(requestCode, resultCode, data, preferences.getInt(SharedPrefsConstants.ORDER_ID, 0));
                final String picturePath = imageUri.getPath();
                File file = new File(picturePath);
                final float length = file.length();
                Log.e("File Length in bytes", String.valueOf(length));
                float lengthinKb = length / 1024;
                Log.e("File Length in kB", String.valueOf(lengthinKb));
                float lengthinmb = lengthinKb / 1024;

                Log.e("File Length  in MB", String.valueOf(lengthinmb));

                if (length <= (imageSize * fileSizeLimit)) {
                    UploadMediaFiles uploadMediaFiles = new UploadMediaFiles();
                    uploadMediaFiles.execute(picturePath);
                } else {
                    Toast.makeText(context, "File Upload limit is" + imageSize + " MB", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == SELECT_AUDIO && resultCode == Activity.RESULT_OK) {
                final Uri audioUri = data.getData();
                final String audioPath = getRealPathFromURI_API19(context, audioUri);
                File file = new File(audioPath);
                final long length = file.length();
                Log.e("File Length ", String.valueOf(length));
                if (length <= (audioSize * fileSizeLimit)) {
                    UploadMediaFiles uploadMediaFiles = new UploadMediaFiles();
                    uploadMediaFiles.execute(audioPath);
                } else {
                    Toast.makeText(context, "File Upload limit is" + audioSize + " MB", Toast.LENGTH_SHORT).show();
                }

            } else if (requestCode == SELECT_VIDEO && resultCode == Activity.RESULT_OK) {
                Uri videoUri = data.getData();
                final String videoPath = getRealPathFromURI_API19Video(context, videoUri);
                File file = new File(videoPath);
                final long length = file.length();
                Log.e("File Length ", String.valueOf(length));
                if (length <= (videoSize * fileSizeLimit)) {
                    UploadMediaFiles uploadMediaFiles = new UploadMediaFiles();
                    uploadMediaFiles.execute(videoPath);
                } else {
                    Toast.makeText(context, "File Upload limit is" + videoSize + " MB", Toast.LENGTH_SHORT).show();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            checkPermission();
        }
    }

    private void callFinalAPI(final ProgressDialog progressDialog, final String type) {
        if (internetStatusHelper.isConnected()) {
            int step = 0;
            switch (type) {
                case "image":
                    step = 2;
                    break;
                case "audio":
                    step = 3;
                    break;
                case "video":
                    step = 4;
                    break;
            }
            Call<StatusResponse> call = WhazdatApplication.getApiEndpointInterface().addPlaceFinalAPI(step, mediaURL, preferences.getInt(SharedPrefsConstants.ORDER_ID, 0), preferences.getString(SharedPrefsConstants.SECRET_HASH_KEY, ""));
            call.enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                    progressDialog.dismiss();
                    if (response.body() != null) {
                        if (response.body().isSuccess()) {
                            //Next Screen
                            if (getActivity() != null) {
                                switch (type) {
                                    case "image":
                                        isImageUploaded = true;
                                        break;
                                    case "audio":
                                        isAudioUploaded = true;
                                        break;
                                    case "video":
                                        isVideoUploaded = true;
                                        break;
                                }
                                checkUploadedMediaAndSet();
                                showCustomDialog("Upload Successful", "Yuppiee! " + type + " has been uploaded", true);

                            }
                        } else {
                            Toast.makeText(getContext(), "Something went wrong!!, please try again later", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Something went wrong!!, please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    if (t != null && t.getMessage() != null) {
                        Toast.makeText(getContext(), "Something went wrong!!, please try again later", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            progressDialog.dismiss();
            internetStatusHelper.showNoInternetToast(context);
        }
    }

    private void showCustomDialog(String title, String subtitle, boolean isSuccess) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final ImageView icon = dialog.findViewById(R.id.icon);
        final TextView titleTv = dialog.findViewById(R.id.title);
        final TextView subTitleTv = dialog.findViewById(R.id.content);
        if (isSuccess) {
            icon.setImageResource(R.drawable.ic_cloud_upload_success);
        } else {
            icon.setImageResource(R.drawable.ic_cloud_upload_fail);
        }

        titleTv.setText(title);
        subTitleTv.setText(subtitle);

        dialog.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public boolean validate() {
        boolean isValidated = false;
        if (isImageAvailable && isAudioAvailable && isVideoAvailable) {
            if (isImageUploaded && isAudioUploaded && isVideoUploaded) {
                isValidated = true;
            }
        } else if (isImageAvailable && isAudioAvailable) {
            if (isImageUploaded && isAudioUploaded) {
                isValidated = true;
            }
        } else if (isImageAvailable && isVideoAvailable) {
            if (isImageUploaded && isVideoUploaded) {
                isValidated = true;
            }
        } else if (isAudioAvailable && isVideoAvailable) {
            if (isAudioUploaded && isVideoUploaded) {
                isValidated = true;
            }
        } else if (isImageAvailable) {
            if (isImageUploaded) {
                isValidated = true;
            }
        } else if (isAudioAvailable) {
            if (isAudioUploaded) {
                isValidated = true;
            }
        } else if (isVideoAvailable) {
            if (isVideoUploaded) {
                isValidated = true;
            }
        }

        return isValidated;
    }

    private void checkUploadedMediaAndSet() {

        if (isAudioUploaded) {
            audioUploaded.setVisibility(View.VISIBLE);
        } else {
            audioUploaded.setVisibility(View.GONE);
        }

        if (isImageUploaded) {
            imageUploaded.setVisibility(View.VISIBLE);
        } else {
            imageUploaded.setVisibility(View.GONE);
        }

        if (isVideoUploaded) {
            videoUploaded.setVisibility(View.VISIBLE);
        } else {
            videoUploaded.setVisibility(View.GONE);
        }


    }

    @SuppressLint("StaticFieldLeak")
    private class UploadMediaFiles extends AsyncTask<String, String, PutObjectResult> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected PutObjectResult doInBackground(String... mediaPath) {
            if (mediaPath[0] == null) {

                return null;
            }

            File file = new File(mediaPath[0]);
            AmazonS3 s3Client;
            ClientConfiguration clientConfig = new ClientConfiguration();
            clientConfig.setProtocol(Protocol.HTTP);
            clientConfig.setMaxErrorRetry(0);
            clientConfig.setSocketTimeout(60000);
            BasicAWSCredentials credentials = new BasicAWSCredentials(AWS_KEY, AWS_SECRET);
            s3Client = new AmazonS3Client(credentials, clientConfig);
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTH_1));

            FileInputStream stream;
            PutObjectResult result = null;
            try {

                stream = new FileInputStream(file);
                ObjectMetadata objectMetadata = new ObjectMetadata();
                Log.d("messge", "converting to bytes");
                objectMetadata.setContentLength(file.length());
                String[] s = mediaPath[0].split("\\.");
                String extension = s[s.length - 1];
                String fileName = preferences.getInt(SharedPrefsConstants.ORDER_ID, 0) + "_" + type;
                PutObjectRequest putObjectRequest = new PutObjectRequest(AWS_BUCKET, "" + fileName + "." + extension, stream, objectMetadata)


                        .withCannedAcl(CannedAccessControlList.PublicRead);
                result = s3Client.putObject(putObjectRequest);
                mediaURL = context.getString(R.string.aws_url) + fileName + "." + extension;
            } catch (Exception e) {
                Log.d("ERROR", " " + e.getMessage());
                e.printStackTrace();
            }
            return result;

        }

        @Override
        protected void onPostExecute(PutObjectResult putObjectResult) {
            super.onPostExecute(putObjectResult);

            if (putObjectResult == null) {
                Log.e("RESULT", "NULL");
                progressDialog.dismiss();
                showCustomDialog("Upload Failed!!!", "Something went wrong with server", false);
                Toast.makeText(context, "Could not upload the media", Toast.LENGTH_SHORT).show();
            } else {
                callFinalAPI(progressDialog, type);
                Log.e("RESULT", putObjectResult.toString());
            }
        }
    }


}
