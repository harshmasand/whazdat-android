package com.whazdat.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.whazdat.R;
import com.whazdat.adapter.MySubscriptionListAdapter;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.ApiEndPointConstants;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.model.MySubscriptionData;
import com.whazdat.model.SubscriptionResponse;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.ErrorView;
import com.whazdat.util.InternetStatusHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 * @author by Harsh Masand on 13/3/18.
 */

public class MySubscriptionFragment extends Fragment {

    ProgressBar loader;
    ErrorView errorView;
    RecyclerView recyclerView;
    InternetStatusHelper internetStatusHelper;
    List<MySubscriptionData> subscriptionDataList;
    MySubscriptionListAdapter mySubscriptionListAdapter;
    SharedPreferences sharedPreferences;
    static final String TAG = MySubscriptionFragment.class.getSimpleName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        internetStatusHelper = new InternetStatusHelper(getContext());
        subscriptionDataList = new ArrayList<>();
        sharedPreferences = WhazdatSharedPrefs.getInstance();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_subscriptions, container, false);
        recyclerView = view.findViewById(R.id.recycle_view_list);
        errorView = view.findViewById(R.id.error_layout_container);
        loader = view.findViewById(R.id.loader);
        fetchMySubscriptions();
        mySubscriptionListAdapter = new MySubscriptionListAdapter(getContext(),subscriptionDataList,R.layout.list_item_subscription);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setHasFixedSize(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mySubscriptionListAdapter);
        errorView.setOnRetryButtonClickListener(new ErrorView.OnRetryButtonClickListener() {
            @Override
            public void onClick() {
                //Retry Button On Click
                fetchMySubscriptions();
            }
        });
        return view;
    }

    /**
     * Fetching subscriptions
     */
    private void fetchMySubscriptions() {
        if(internetStatusHelper.isConnected()){
            showLoaderView();
            Call<SubscriptionResponse> call = WhazdatApplication.getApiEndpointInterface().getSubscriptionList(ApiEndPointConstants.SUBSCRIPTION,sharedPreferences.getInt(SharedPrefsConstants.ID,0));
            call.enqueue(new Callback<SubscriptionResponse>() {
                @Override
                public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                    if(response.body() != null ){
                        SubscriptionResponse apiResponse = response.body();
                        if(response.body().isSuccess()){
                            if(apiResponse.getData() != null){
                                ArrayList<MySubscriptionData> mySubscriptionDataList = apiResponse.getData();
                                if(mySubscriptionDataList != null && !mySubscriptionDataList.isEmpty()) {
                                    subscriptionDataList.clear();
                                    subscriptionDataList.addAll(mySubscriptionDataList);
                                    mySubscriptionListAdapter.notifyDataSetChanged();
                                    showDataView();
                                }

                            }else{
                                showNoDataAvailableView();
                            }
                        }else{
                          showNoDataAvailableView();
                        }
                    }else{
                        showErrorView();
                    }
                }

                @Override
                public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                    if(t !=null && t.getMessage() !=null){
                        Log.e(TAG,t.getMessage()+"");
                    }
                    showErrorView();
                }
            });
        }else{
            showNoInternetView();
        }
    }

    public void showLoaderView(){
        loader.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }

    public void showDataView(){
        loader.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    public void showNoDataAvailableView(){
        showDataInfo(getString(R.string.no_results_text), getString(R.string.no_data_message),false);
    }

    public void showErrorView(){
        showDataInfo(getString(R.string.oops_text), getString(R.string.something_went_wrong_text),true);
    }

    public void showNoInternetView(){
        showDataInfo(getString(R.string.oops_text),getString(R.string.you_are_offline_text),true);
    }

    private void showDataInfo(String title, String subTitle,boolean showRetryButton) {
        errorView.changeTitleText(title);
        errorView.changeSubtitleText(subTitle);
        if (showRetryButton) {
            errorView.showButton();
        }else {
            errorView.hideButton();
        }
        loader.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }

    public void refreshData(){
        fetchMySubscriptions();
    }
}
