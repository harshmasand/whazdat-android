package com.whazdat.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.whazdat.R;
import com.whazdat.activity.HomeActivity;
import com.whazdat.activity.MyPlacesInnerActivity;
import com.whazdat.adapter.PlacesListAdapter;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.ApiEndPointConstants;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.constants.WhazdatIntentConstants;
import com.whazdat.listener.RecyclerItemListener;
import com.whazdat.model.PlaceListResponse;
import com.whazdat.model.PlacesData;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.ErrorView;
import com.whazdat.util.InternetStatusHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.whazdat.fragments.MySubscriptionFragment.TAG;

/**
 * @author by Harsh Masand on 11/3/18.
 */

public class PlacesFragment extends Fragment {

    ProgressBar loader;
    ErrorView errorView;
    RecyclerView recyclerView;
    InternetStatusHelper internetStatusHelper;
    List<PlacesData> list;
    PlacesListAdapter placesListAdapter;
    SharedPreferences sharedPreferences;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = WhazdatSharedPrefs.getInstance();
        internetStatusHelper = new InternetStatusHelper(getContext());
        list = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_places, container, false);
        recyclerView = view.findViewById(R.id.recycle_view_list);
        errorView = view.findViewById(R.id.error_layout_container);
        loader = view.findViewById(R.id.loader);
        placesListAdapter = new PlacesListAdapter(getContext(),R.layout.list_item_places,list);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(placesListAdapter);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.addOnItemTouchListener(new RecyclerItemListener(getContext(), recyclerView, new RecyclerItemListener.RecyclerTouchListener() {
            @Override
            public void onClickItem(View v, int position) {
                PlacesData placesDataItem = list.get(position);
                Intent intent = new Intent(getActivity(), MyPlacesInnerActivity.class);
                intent.putExtra(WhazdatIntentConstants.ORDER_ID,placesDataItem.getOrderID());
                startActivity(intent);
            }

            @Override
            public void onLongClickItem(View v, int position) {
                //Do Nothing
            }
        }));
        errorView.setOnRetryButtonClickListener(new ErrorView.OnRetryButtonClickListener() {
            @Override
            public void onClick() {
                fetchPlacesList();
            }
        });
        fetchPlacesList();
        return view;
    }

    public void fetchPlacesList() {
        if(internetStatusHelper.isConnected()){
            showLoaderView();
            Call<PlaceListResponse> call = WhazdatApplication.getApiEndpointInterface().getPlaceList(ApiEndPointConstants.PLACE_LIST,sharedPreferences.getInt(SharedPrefsConstants.ID,0),WhazdatApplication.getApplication().getLatitude(),WhazdatApplication.getApplication().getLongitude());
            call.enqueue(new Callback<PlaceListResponse>() {
                @Override
                public void onResponse(Call<PlaceListResponse> call, Response<PlaceListResponse> response) {
                    if(response.body() != null){
                        PlaceListResponse apiResponse = response.body();
                        if(apiResponse.isSuccess()){
                            ArrayList<PlacesData> placesDataList = apiResponse.getData();
                            if(placesDataList != null && !placesDataList.isEmpty()) {
                                list.clear();
                                list.addAll(placesDataList);
                                placesListAdapter.notifyDataSetChanged();
                                showDataView();
                            }else{
                                showNoDataAvailableView();
                            }
                        }else{
                            showNoDataAvailableView();
                        }
                    }else{
                        showErrorView();
                    }
                }

                @Override
                public void onFailure(Call<PlaceListResponse> call, Throwable t) {
                    if(t != null && t.getMessage() != null){
                        Log.e(TAG,t.getMessage()+"");
                    }
                    showErrorView();
                }
            });
        }else{
            showNoInternetView();
        }
    }

    public void showLoaderView(){
        loader.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }

    public void showDataView(){
        loader.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    public void showNoDataAvailableView(){
        showDataInfo(getString(R.string.no_results_text), getString(R.string.no_data_message),false);
    }

    public void showErrorView(){
        showDataInfo(getString(R.string.oops_text), getString(R.string.something_went_wrong_text),true);
    }

    public void showNoInternetView(){
        showDataInfo(getString(R.string.oops_text),getString(R.string.you_are_offline_text),true);
    }

    private void showDataInfo(String title, String subTitle, boolean showRetryButton) {
        errorView.changeTitleText(title);
        errorView.changeSubtitleText(subTitle);
        if (showRetryButton) {
            errorView.showButton();
        }else {
            errorView.hideButton();
        }
        loader.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() instanceof HomeActivity) {
            fetchPlacesList();
            ((HomeActivity) getActivity()).changeToolbarTitleAndSubtitle("Discover Legacies","");
        }
    }

}
