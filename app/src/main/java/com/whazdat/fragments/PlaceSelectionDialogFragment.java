package com.whazdat.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.whazdat.R;
import com.whazdat.adapter.PlaceSubscriptionAdapter;
import com.whazdat.listener.RecyclerItemListener;
import com.whazdat.model.PlacePackageModel;

import java.util.List;

public class PlaceSelectionDialogFragment extends DialogFragment {


    List<PlacePackageModel> list;
    Context context;
    PlaceSubscriptionAdapter placeSubscriptionAdapter;
    int lastPositionClicked;
    PlacePackageModel placesDataItem;
    SubscriptionSelectionListener subscriptionSelectionListener;

    public PlaceSelectionDialogFragment() {
        //Empty Constructor
    }

    @SuppressLint("ValidFragment")
    public PlaceSelectionDialogFragment(List<PlacePackageModel> list, SubscriptionSelectionListener subscriptionSelectionListener) {
        this.list = list;
        this.subscriptionSelectionListener = subscriptionSelectionListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @NonNull
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();

        if (dialog != null) {

            WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
            wmlp.width = WindowManager.LayoutParams.MATCH_PARENT;
            wmlp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setAttributes(wmlp);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.place_selection_dialog, container, false);
        final Button selectButton = view.findViewById(R.id.select_btn);
        Button cancelButton = view.findViewById(R.id.cancel_btn);
        final RecyclerView recyclerView = view.findViewById(R.id.place_list);
        placeSubscriptionAdapter = new PlaceSubscriptionAdapter(context, R.layout.list_item_subcription, list);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setHasFixedSize(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(placeSubscriptionAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemListener(getContext(), recyclerView, new RecyclerItemListener.RecyclerTouchListener() {
            @Override
            public void onClickItem(View v, int position) {
                list.get(lastPositionClicked).setChecked(false);
                list.get(position).setChecked(true);
                lastPositionClicked = position;
                placesDataItem = list.get(position);
                placeSubscriptionAdapter.notifyDataSetChanged();

            }

            @Override
            public void onLongClickItem(View v, int position) {
                //Do Nothing
            }
        }));
        selectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Send the selected Object
                subscriptionSelectionListener.onPlaceSubscriptionSelection(placesDataItem);
                dismissAllowingStateLoss();

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissAllowingStateLoss();
                if (getActivity() != null) {
                    getActivity().finish();
                }
            }
        });
        return view;

    }

    public interface SubscriptionSelectionListener {
        void onPlaceSubscriptionSelection(PlacePackageModel placePackageModel);
    }


}
