package com.whazdat.fragments;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.whazdat.R;
import com.whazdat.activity.HomeActivity;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.ApiEndPointConstants;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.model.BookmarkData;
import com.whazdat.model.ProfileData;
import com.whazdat.model.UserData;
import com.whazdat.model.UserProfileResponse;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.InternetStatusHelper;

import java.util.Observable;
import java.util.Observer;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author by Harsh Masand on 12/3/18.
 */

public class MyProfileFragment extends Fragment implements Observer{

    private static final String TAG = MyProfileFragment.class.getSimpleName();
    RelativeLayout profileContainer;
    TextView nameTv,descriptionTv,myPlacesTv,subscriptionsTv,viewsTv,ageTv;
    CircleImageView profileImage;
    TabLayout tabs;
    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;
    ViewPager viewPager;
    MyPlacesFragment myPlacesFragment;
    MySubscriptionFragment mySubscriptionFragment;
    InternetStatusHelper internetStatusHelper;
    SharedPreferences sharedPreferences;
    BookmarkData bookmarkData;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        internetStatusHelper = new InternetStatusHelper(getContext());
        sharedPreferences = WhazdatSharedPrefs.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        //Instantiated the Observable
        bookmarkData = BookmarkData.getInstance();
        //add the Observer to watch for all the bookmark changes
        bookmarkData.addObserver(this);
        profileContainer = view.findViewById(R.id.profile_container);
        nameTv = view.findViewById(R.id.name);
        descriptionTv = view.findViewById(R.id.description);
        myPlacesTv = view.findViewById(R.id.my_places_value);
        subscriptionsTv = view.findViewById(R.id.my_subscriptions_value);
        viewsTv = view.findViewById(R.id.my_views_value);
        ageTv = view.findViewById(R.id.age);
        profileImage = view.findViewById(R.id.profile_image);
        tabs = view.findViewById(R.id.tabs);
        viewPager = view.findViewById(R.id.pager);
        collapsingToolbarLayout = view.findViewById(R.id.collapsing_toolbar);
        appBarLayout = view.findViewById(R.id.appbar_layout);
        myPlacesFragment = new MyPlacesFragment();
        mySubscriptionFragment = new MySubscriptionFragment();
        setViewPager();
        fetchProfileData();
        return view;
    }

    @SuppressWarnings("deprecation")
    public void fetchProfileData() {
        if(internetStatusHelper.isConnected()) {
            final ProgressDialog pd = new ProgressDialog(getContext());
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.show();
            Call<UserProfileResponse> call = WhazdatApplication.getApiEndpointInterface().showUserProfileData(ApiEndPointConstants.PROFILE,sharedPreferences.getInt(SharedPrefsConstants.ID,0));
            call.enqueue(new Callback<UserProfileResponse>() {
                @Override
                public void onResponse(Call<UserProfileResponse> call, Response<UserProfileResponse> response) {
                    pd.dismiss();
                    if(response.body() !=null){
                        if(response.body().isSuccess()){
                            if(response.body().getData() != null) {
                                setProfileComponents(response.body().getData());
                            }
                        }else{
                            Toast.makeText(getContext(),getString(R.string.something_went_wrong),Toast.LENGTH_SHORT).show();    
                        }
                    }else{
                        Toast.makeText(getContext(),getString(R.string.something_went_wrong),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserProfileResponse> call, Throwable t) {
                    pd.dismiss();
                    if(t !=null && t.getMessage() != null){
                        Log.e(TAG,t.getMessage()+"");
                    }
                    Toast.makeText(getContext(),getString(R.string.something_went_wrong),Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            internetStatusHelper.showNoInternetToast(getContext());
        }
    }
    /**
     * @param userData
     * **/
    private void setProfileComponents(UserData userData) {
        ProfileData profileData = userData.getProfile();
        if(profileData != null) {
            nameTv.setText(profileData.getDisplay_name());
            //TODO : Description Pending
            descriptionTv.setText("Canada");
            ageTv.setText(String.valueOf(profileData.getAge()));
            Picasso.with(getContext())
                    .load(profileData.getImage())
                    .fit()
                    .placeholder(R.drawable.man)
                    .into(profileImage);
        }
        myPlacesTv.setText(String.valueOf(userData.getBookmarkCount()));
        viewsTv.setText(String.valueOf(userData.getViewedCount()));
        subscriptionsTv.setText(String.valueOf(userData.getSubscriptionCount()));
    }

    private void setViewPager() {
        viewPager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return myPlacesFragment;
                    case 1:
                        return mySubscriptionFragment;
                  default:
                        return myPlacesFragment;
                }
            }

            @Override
            public int getCount() {
                return 2;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return "My Places";
                    case 1:
                        return "My Subscription";
                    default:
                        return "";
                }
            }

        });
        viewPager.setOffscreenPageLimit(1);
        tabs.setupWithViewPager(viewPager);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).changeToolbarTitleAndSubtitle("My Profile", "");
        }
    }

    public void setCurrentItem(int position){
        viewPager.setCurrentItem(position,true);
    }

    @Override
    public void update(Observable observable, Object o) {
        if(observable != null && observable instanceof BookmarkData){
            if(myPlacesFragment != null && myPlacesFragment.isAdded()){
                myPlacesFragment.refreshData();

            }
        }
    }
}
