package com.whazdat.model;

/**
 * @author by Harsh Masand on 18/6/18.
 */
public class SetBookmarkResponse {
    private boolean success;
    private BookmarkData data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public BookmarkData getData() {
        return data;
    }

    public void setData(BookmarkData data) {
        this.data = data;
    }


}
