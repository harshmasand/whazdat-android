package com.whazdat.model;

import java.util.ArrayList;

/**
 * @author by Harsh Masand on 3/7/18.
 */
public class BookmarkedPlacesResponse {
    private boolean success;
    private ArrayList<PlacesData> data;
    private Error error;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<PlacesData> getData() {
        return data;
    }

    public void setData(ArrayList<PlacesData> data) {
        this.data = data;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
