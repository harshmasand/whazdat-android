package com.whazdat.model;

import java.util.ArrayList;

/**
 * @author by Harsh Masand on 18/6/18.
 */
public class SubscriptionResponse {

    private boolean success;
    private ArrayList<MySubscriptionData> data;
    private Error error;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<MySubscriptionData> getData() {
        return data;
    }

    public void setData(ArrayList<MySubscriptionData> data) {
        this.data = data;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
