package com.whazdat.model;

import com.google.gson.annotations.SerializedName;

import java.util.Observable;

/**
 * @author by Harsh Masand on 18/6/18.
 */
public class BookmarkData extends Observable{

    private static Object mLock = new Object();

    private static BookmarkData BOOKMARK_DATA_INSTANCE = null;

    @SerializedName("is_bookmarked")
    private boolean bookmark;

    public boolean isBookmark() {
        return bookmark;
    }

    public BookmarkData setBookmark(boolean bookmark) {
        this.bookmark = bookmark;
        setChanged();
        notifyObservers();
        return this;
    }

    private BookmarkData(){
        //Empty Constructor
    }

    public static BookmarkData getInstance(){
        synchronized (mLock){
            if(BOOKMARK_DATA_INSTANCE == null){
                BOOKMARK_DATA_INSTANCE = new BookmarkData();
            }
            return BOOKMARK_DATA_INSTANCE;
        }
    }

}
