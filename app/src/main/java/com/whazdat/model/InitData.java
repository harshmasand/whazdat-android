package com.whazdat.model;

import com.google.gson.annotations.SerializedName;

public class InitData {
    @SerializedName("order_id")
    private int orderID;

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }
}
