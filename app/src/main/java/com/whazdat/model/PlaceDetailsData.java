package com.whazdat.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author by Harsh Masand on 18/6/18.
 */
public class PlaceDetailsData {

    @SerializedName("place_name")
    private String placeName;

    @SerializedName("place_about")
    private String placeDescription;

    @SerializedName("place_image_url")
    private String placeImageURL;

    @SerializedName("place_audio_url")
    private String placeAudioURL;

    @SerializedName("place_video_url")
    private String placeVideoURL;

    @SerializedName("is_bookmarked")
    private boolean bookmark;

    @SerializedName("distance")
    private String distance;

    @SerializedName("place_birth_date")
    private String dob;

    @SerializedName("place_latitude")
    private double latitude;

    @SerializedName("place_longitude")
    private double longitude;

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceDescription() {
        return placeDescription;
    }

    public void setPlaceDescription(String placeDescription) {
        this.placeDescription = placeDescription;
    }

    public String getPlaceImageURL() {
        return placeImageURL;
    }

    public void setPlaceImageURL(String placeImageURL) {
        this.placeImageURL = placeImageURL;
    }

    public String getPlaceAudioURL() {
        return placeAudioURL;
    }

    public void setPlaceAudioURL(String placeAudioURL) {
        this.placeAudioURL = placeAudioURL;
    }

    public String getPlaceVideoURL() {
        return placeVideoURL;
    }

    public void setPlaceVideoURL(String placeVideoURL) {
        this.placeVideoURL = placeVideoURL;
    }

    public boolean isBookmark() {
        return bookmark;
    }

    public void setBookmark(boolean bookmark) {
        this.bookmark = bookmark;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
}
