package com.whazdat.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author by Harsh Masand on 2/7/18.
 */
public class LocationSearchData {

    @SerializedName("place_name")
    private String placeName;

    @SerializedName("order_id")
    private int orderID;

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }
}
