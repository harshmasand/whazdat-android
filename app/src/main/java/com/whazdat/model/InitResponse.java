package com.whazdat.model;

public class InitResponse {
    private boolean success;
    private InitData data;
    private Error error;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public InitData getData() {
        return data;
    }

    public void setData(InitData data) {
        this.data = data;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
