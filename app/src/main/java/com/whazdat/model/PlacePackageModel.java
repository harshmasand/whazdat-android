package com.whazdat.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PlacePackageModel implements Parcelable {

    public static final Creator<PlacePackageModel> CREATOR = new Creator<PlacePackageModel>() {
        @Override
        public PlacePackageModel createFromParcel(Parcel in) {
            return new PlacePackageModel(in);
        }

        @Override
        public PlacePackageModel[] newArray(int size) {
            return new PlacePackageModel[size];
        }
    };
    boolean checked;
    private int id;
    @SerializedName("order_id")
    private
    int orderID;
    @SerializedName("secret_key_hash")
    private
    String secretKeyHash;
    @SerializedName("place_image_size")
    private
    int imageSize;
    @SerializedName("place_video_size")
    private
    int videoSize;
    @SerializedName("place_audio_size")
    private
    int audioSize;
    @SerializedName("place_subscription_name")
    private
    String subscriptionName;

    protected PlacePackageModel(Parcel in) {
        id = in.readInt();
        orderID = in.readInt();
        secretKeyHash = in.readString();
        imageSize = in.readInt();
        videoSize = in.readInt();
        audioSize = in.readInt();
        subscriptionName = in.readString();
        checked = in.readByte() != 0;
    }

    PlacePackageModel() {
        //Empty Constructor
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(orderID);
        dest.writeString(secretKeyHash);
        dest.writeInt(imageSize);
        dest.writeInt(videoSize);
        dest.writeInt(audioSize);
        dest.writeString(subscriptionName);
        dest.writeByte((byte) (checked ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getSecretKeyHash() {
        return secretKeyHash;
    }

    public void setSecretKeyHash(String secretKeyHash) {
        this.secretKeyHash = secretKeyHash;
    }

    public int getImageSize() {
        return imageSize;
    }

    public void setImageSize(int imageSize) {
        this.imageSize = imageSize;
    }

    public int getVideoSize() {
        return videoSize;
    }

    public void setVideoSize(int videoSize) {
        this.videoSize = videoSize;
    }

    public int getAudioSize() {
        return audioSize;
    }

    public void setAudioSize(int audioSize) {
        this.audioSize = audioSize;
    }

    public String getSubscriptionName() {
        return subscriptionName;
    }

    public void setSubscriptionName(String subscriptionName) {
        this.subscriptionName = subscriptionName;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
