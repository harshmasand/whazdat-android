package com.whazdat.model;

import java.util.ArrayList;

/**
 * @author by Harsh Masand on 26/6/18.
 */
public class LatLongListResponse {
    private boolean success;
    private ArrayList<LatLngData> data;
    private Error error;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<LatLngData> getData() {
        return data;
    }

    public void setData(ArrayList<LatLngData> data) {
        this.data = data;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
