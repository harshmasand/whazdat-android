package com.whazdat.model;


import com.google.gson.annotations.SerializedName;

/**
 * @author by Harsh Masand on 29/6/18.
 */
public class UserData {
    private ProfileData profile;

    @SerializedName("bookmark_count")
    private int bookmarkCount;

    @SerializedName("viewed_count")
    private int viewedCount;

    @SerializedName("subscription_count")
    private int subscriptionCount;

    public ProfileData getProfile() {
        return profile;
    }

    public void setProfile(ProfileData profile) {
        this.profile = profile;
    }

    public int getBookmarkCount() {
        return bookmarkCount;
    }

    public void setBookmarkCount(int bookmarkCount) {
        this.bookmarkCount = bookmarkCount;
    }

    public int getViewedCount() {
        return viewedCount;
    }

    public void setViewedCount(int viewedCount) {
        this.viewedCount = viewedCount;
    }

    public int getSubscriptionCount() {
        return subscriptionCount;
    }

    public void setSubscriptionCount(int subscriptionCount) {
        this.subscriptionCount = subscriptionCount;
    }
}
