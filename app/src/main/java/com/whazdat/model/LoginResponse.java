package com.whazdat.model;

/**
 * @author by Harsh Masand on 8/6/18.
 */

public class LoginResponse {

    private boolean success;
    private String message;
    private LoginData data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }


}
