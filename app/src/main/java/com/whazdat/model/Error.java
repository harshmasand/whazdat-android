package com.whazdat.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author by Harsh Masand on 8/6/18.
 */

public class Error {
    @SerializedName("reason")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
