package com.whazdat.model;

import java.util.ArrayList;
import java.util.List;

public class AddPlacePackageResponse {
    List<PlacePackageModel> data = new ArrayList<>();
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<PlacePackageModel> getData() {
        return data;
    }

    public void setData(List<PlacePackageModel> data) {
        this.data = data;
    }
}
