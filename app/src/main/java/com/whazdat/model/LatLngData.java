package com.whazdat.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author by Harsh Masand on 4/6/18.
 */

public class LatLngData implements Parcelable {

    @SerializedName("place_latitude")
    private double latitude;

    @SerializedName("place_longitude")
    private double longitude;

    @SerializedName("order_id")
    private int orderID;

    @SerializedName("place_name")
    private String placeName;

    public LatLngData() {
        //Empty
    }

    protected LatLngData(Parcel in) {
        latitude = in.readDouble();
        longitude = in.readDouble();
        orderID = in.readInt();
        placeName = in.readString();
    }

    public static final Creator<LatLngData> CREATOR = new Creator<LatLngData>() {
        @Override
        public LatLngData createFromParcel(Parcel in) {
            return new LatLngData(in);
        }

        @Override
        public LatLngData[] newArray(int size) {
            return new LatLngData[size];
        }
    };

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeInt(orderID);
        dest.writeString(placeName);
    }
}
