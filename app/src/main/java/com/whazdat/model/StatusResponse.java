package com.whazdat.model;

/**
 * @author by Harsh Masand on 18/6/18.
 */
public class StatusResponse {
    private boolean success;
    private Error error;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
