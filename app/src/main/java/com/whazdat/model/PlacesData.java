package com.whazdat.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author by Harsh Masand on 30/3/18.
 */

public class PlacesData implements Parcelable{

    @SerializedName("place_image_url")
    private String thumbnail;

    @SerializedName("place_name")
    private String placeName;

    @SerializedName("place_birth_date")
    private String dob;

    private String distance;

    @SerializedName("order_id")
    private int orderID;

    protected PlacesData(Parcel in) {
        thumbnail = in.readString();
        placeName = in.readString();
        dob = in.readString();
        distance = in.readString();
        orderID = in.readInt();
    }

    public PlacesData(){
        //Empty Constructor
    }

    public static final Creator<PlacesData> CREATOR = new Creator<PlacesData>() {
        @Override
        public PlacesData createFromParcel(Parcel in) {
            return new PlacesData(in);
        }

        @Override
        public PlacesData[] newArray(int size) {
            return new PlacesData[size];
        }
    };

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(thumbnail);
        dest.writeString(placeName);
        dest.writeString(dob);
        dest.writeString(distance);
        dest.writeInt(orderID);
    }
}
