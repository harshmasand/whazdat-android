package com.whazdat.model;

/**
 * @author by Harsh Masand on 13/6/18.
 */

public class UserProfileResponse {
    private boolean success;
    private UserData data;
    private Error error;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
