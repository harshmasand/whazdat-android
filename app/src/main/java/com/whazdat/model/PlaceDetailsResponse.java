package com.whazdat.model;

/**
 * @author by Harsh Masand on 18/6/18.
 */
public class                                                                                                                                                                                                                    PlaceDetailsResponse {
    private boolean success;
    private PlaceDetailsData data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public PlaceDetailsData getData() {
        return data;
    }

    public void setData(PlaceDetailsData data) {
        this.data = data;
    }


}
