package com.whazdat.model;

import java.util.ArrayList;

/**
 * @author by Harsh Masand on 2/7/18.
 */
public class LocationSearchResponse {
    private boolean success;
    private ArrayList<LocationSearchData> data;
    private Error error;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<LocationSearchData> getData() {
        return data;
    }

    public void setData(ArrayList<LocationSearchData> data) {
        this.data = data;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
