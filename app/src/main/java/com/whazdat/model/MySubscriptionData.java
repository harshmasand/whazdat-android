package com.whazdat.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author by Harsh Masand on 16/3/18.
 */

public class MySubscriptionData {

    @SerializedName("order_id")
    private int orderID;

    @SerializedName("place_subscription_name")
    private String subscriptionName;

    @SerializedName("bought_on")
    private String boughtOn;

    @SerializedName("place_image_size")
    private String imageSize;

    @SerializedName("place_audio_size")
    private String audioSize;

    @SerializedName("place_video_size")
    private String videoSize;

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getSubscriptionName() {
        return subscriptionName;
    }

    public void setSubscriptionName(String subscriptionName) {
        this.subscriptionName = subscriptionName;
    }

    public String getBoughtOn() {
        return boughtOn;
    }

    public void setBoughtOn(String boughtOn) {
        this.boughtOn = boughtOn;
    }

    public String getImageSize() {
        return imageSize;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }

    public String getAudioSize() {
        return audioSize;
    }

    public void setAudioSize(String audioSize) {
        this.audioSize = audioSize;
    }

    public String getVideoSize() {
        return videoSize;
    }

    public void setVideoSize(String videoSize) {
        this.videoSize = videoSize;
    }
}
