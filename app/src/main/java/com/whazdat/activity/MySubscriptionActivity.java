package com.whazdat.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.whazdat.R;
import com.whazdat.adapter.MySubscriptionListAdapter;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.ApiEndPointConstants;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.model.MySubscriptionData;
import com.whazdat.model.SubscriptionResponse;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.ErrorView;
import com.whazdat.util.InternetStatusHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author by Harsh Masand on 8/4/18.
 */

public class MySubscriptionActivity extends AppCompatActivity {

    static final String TAG = MySubscriptionActivity.class.getSimpleName();
    ProgressBar loader;
    ErrorView errorView;
    RecyclerView recyclerView;
    InternetStatusHelper internetStatusHelper;
    List<MySubscriptionData> subscriptionDataList;
    MySubscriptionListAdapter mySubscriptionListAdapter;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        internetStatusHelper = new InternetStatusHelper(getApplicationContext());
        subscriptionDataList = new ArrayList<>();
        sharedPreferences = WhazdatSharedPrefs.getInstance();
        recyclerView = findViewById(R.id.recycle_view_list);
        errorView = findViewById(R.id.error_layout_container);
        loader = findViewById(R.id.loader);
        fetchMySubscriptions();
        mySubscriptionListAdapter = new MySubscriptionListAdapter(getApplicationContext(),subscriptionDataList,R.layout.list_item_subscription);
        recyclerView.setNestedScrollingEnabled(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mySubscriptionListAdapter);
        errorView.setOnRetryButtonClickListener(new ErrorView.OnRetryButtonClickListener() {
            @Override
            public void onClick() {
                //Retry Button On Click
                fetchMySubscriptions();
            }
        });
    }

    private void fetchMySubscriptions() {
        if(internetStatusHelper.isConnected()){
            showLoaderView();
            Call<SubscriptionResponse> call = WhazdatApplication.getApiEndpointInterface().getSubscriptionList(ApiEndPointConstants.SUBSCRIPTION,sharedPreferences.getInt(SharedPrefsConstants.ID,0));
            call.enqueue(new Callback<SubscriptionResponse>() {
                @Override
                public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                    if(response.body() != null ){
                        SubscriptionResponse apiResponse = response.body();
                        if(apiResponse.getData() != null){
                            ArrayList<MySubscriptionData> mySubscriptionDataList = apiResponse.getData();
                            if(mySubscriptionDataList != null && !mySubscriptionDataList.isEmpty()) {
                                subscriptionDataList.clear();
                                subscriptionDataList.addAll(mySubscriptionDataList);
                                mySubscriptionListAdapter.notifyDataSetChanged();
                                showDataView();
                            }

                        }else {
                            showNoDataAvailableView();
                        }
                    }else{
                        showErrorView();
                    }
                }

                @Override
                public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                    if(t !=null && t.getMessage() !=null){
                        Log.e(TAG,t.getMessage()+"");
                    }
                    showErrorView();
                }
            });
        }else{
            showNoInternetView();
        }
    }

    public void showLoaderView(){
        loader.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }

    public void showDataView(){
        loader.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    public void showNoDataAvailableView(){
        showDataInfo("Something went wrong! Please try again", false);
    }

    public void showErrorView(){
        showDataInfo("Something went wrong! Please try again", true);
    }

    public void showNoInternetView(){
        showDataInfo("No Internet Connection",true);
    }

    private void showDataInfo(String message, boolean showRetryButton) {
        errorView.changeTitleText(message);
        if (showRetryButton) {
            errorView.showButton();
        }else {
            errorView.hideButton();
        }
        loader.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }
}
