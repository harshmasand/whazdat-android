package com.whazdat.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;
import com.whazdat.R;
import com.whazdat.adapter.DrawerAdapter;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.fragments.MyProfileFragment;
import com.whazdat.fragments.NearByFragment;
import com.whazdat.fragments.PlacesFragment;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.InternetStatusHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author by Harsh Masand on 9/3/18.
 */

public class HomeActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    AHBottomNavigationViewPager bottomNavViewpager;
    AHBottomNavigation bottomNav;
    InternetStatusHelper internetStatusHelper;
    PlacesFragment placesFragment;
    NearByFragment nearByFragment;
    MyProfileFragment myProfileFragment;
    TextView versionText;
    Toolbar toolbar;
    TextView toolbarTitle,toolbarSubTitle,userName,emailAddress;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ListView navList;
    static String navArray[] = {"Add Place","My Places","My Subscriptions","Logout"};
    List<String> staticNavList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        sharedPreferences = WhazdatSharedPrefs.getInstance();
        editor = WhazdatSharedPrefs.getEditor();
        //Initiate Toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarSubTitle = findViewById(R.id.toolbar_subTitle);
        navList = findViewById(R.id.nav_list);
        if (toolbar != null && toolbarTitle != null) {
            toolbar.setTitle("");
            toolbarTitle.setText(getString(R.string.discover_legacies));
            toolbarSubTitle.setText("");
            if(toolbarSubTitle.getText().toString().equalsIgnoreCase("")){
                toolbarSubTitle.setVisibility(View.GONE);
            }else{
                toolbarSubTitle.setVisibility(View.VISIBLE);
            }
            toolbar.setNavigationIcon(R.drawable.ic_menu_black_48dp);
        }
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        internetStatusHelper = new InternetStatusHelper(this);
        bottomNav = findViewById(R.id.bottom_navigation_container);
        bottomNavViewpager = findViewById(R.id.bottomNavViewPager);
        drawerLayout = findViewById(R.id.drawer_layout);
        userName = findViewById(R.id.profile_name);
        emailAddress = findViewById(R.id.email_address);
        userName.setText(sharedPreferences.getString(SharedPrefsConstants.USER_NAME,"Test User"));
        emailAddress.setText(sharedPreferences.getString(SharedPrefsConstants.USER_EMAIL_ADDRESS,"test@whazdat.ca"));
        versionText = findViewById(R.id.version_text);
        PackageInfo pInfo;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionText.setText("Version : " + pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            versionText.setVisibility(View.GONE);
        }
        populateNavList();
        final List<String> navItems = new ArrayList<>(staticNavList);
        DrawerAdapter drawerAdapter = new DrawerAdapter(this, navItems);
        if(navList != null){
            navList.setAdapter(drawerAdapter);
        }
        setupNavDrawer();
        if(navList != null){
            navList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    if(navItems.get(i).equalsIgnoreCase("Notifications")){
                        //Do Nothing
                    }else if(navItems.get(i).equalsIgnoreCase("My Places")){
                        drawerLayout.closeDrawers();
                        gotoHomeFragment(getString(R.string.profile),true);
                        myProfileFragment.setCurrentItem(0);
                    }else if(navItems.get(i).equalsIgnoreCase("My Subscriptions")){
                        drawerLayout.closeDrawers();
                        gotoHomeFragment(getString(R.string.profile),true);
                        myProfileFragment.setCurrentItem(1);
                    }else if(navItems.get(i).equalsIgnoreCase("Logout")){
                        openLogoutDialog();
                    } else if(navItems.get(i).equalsIgnoreCase("Add Place")) {
                        // Open Add a Place Activity
                        drawerLayout.closeDrawers();
                        startActivity(new Intent(getApplicationContext(),AddPlaceActivity.class));
                    }
                }
            });
        }
        createNavItems(bottomNav,bottomNavViewpager);

    }

    private void populateNavList() {
        staticNavList.clear();
        staticNavList.addAll(Arrays.asList(navArray));
    }

    private void openLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle(getString(R.string.logout));
        builder.setMessage(getString(R.string.sure_logout));
        builder.setCancelable(true);
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editor.putBoolean(SharedPrefsConstants.LOGGED_IN,false).apply();
                Intent loginIntent = new Intent(HomeActivity.this, LoginActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginIntent);
                finish();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void setupNavDrawer() {
         drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                supportInvalidateOptionsMenu();// creates call to onPrepareOptionsMenu()
                //invalidateOptionsMenu();
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                //invalidateOptionsMenu();
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.addDrawerListener(drawerToggle);
    }

    private void createNavItems(AHBottomNavigation bottomNav, final AHBottomNavigationViewPager bottomNavViewpager) {
        final List<Fragment> fragments = new ArrayList<>();
        final List<AHBottomNavigationItem> bottomNavItems = new ArrayList<>();
        bottomNav.setForceTitlesDisplay(false);
        AHBottomNavigationItem placesItem = new AHBottomNavigationItem(getString(R.string.places), R.drawable.ic_compass);
        AHBottomNavigationItem nearByItem = new AHBottomNavigationItem(getString(R.string.near_by), R.drawable.ic_location);
        AHBottomNavigationItem myProfileItem = new AHBottomNavigationItem(getString(R.string.profile), R.drawable.ic_person);
        //Add items to bottom navigation
        bottomNavItems.add(placesItem);
        bottomNavItems.add(nearByItem);
        bottomNavItems.add(myProfileItem);
        placesFragment = new PlacesFragment();
        fragments.add(placesFragment);
        nearByFragment = new NearByFragment();
        fragments.add(nearByFragment);
        myProfileFragment = new MyProfileFragment();
        fragments.add(myProfileFragment);

        bottomNav.addItems(bottomNavItems);
        bottomNav.setAccentColor(ContextCompat.getColor(this, R.color.button_bg));
        bottomNavViewpager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }
        });
        bottomNav.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                bottomNavViewpager.setCurrentItem(position, true);
                return true;
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.search_item) {
            openSearchActivity();
            return super.onOptionsItemSelected(item);
        }else if (drawerToggle != null) {
            return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
        } else{
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (drawerToggle != null) {
            drawerToggle.syncState();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    private void openSearchActivity() {
        Intent searchIntent = new Intent(HomeActivity.this,SearchActivity.class);
        startActivity(searchIntent);
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle(getString(R.string.alert));
        builder.setMessage(getString(R.string.sure_exit));
        builder.setCancelable(true);
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void changeToolbarTitleAndSubtitle(String title, String subTitle) {
        if (toolbarTitle != null && toolbarSubTitle != null) {
            toolbarTitle.setText(title);
            if(!subTitle.equalsIgnoreCase("")) {
                toolbarSubTitle.setVisibility(View.VISIBLE);
                toolbarSubTitle.setText(subTitle);
            }else{
                toolbarSubTitle.setVisibility(View.GONE);
            }
        }
    }

    public void gotoHomeFragment(String targetFragmentTitle,boolean refreshFragmentData){
        if(bottomNavViewpager.getAdapter().getCount() == 0){
            Intent dataIntent = getIntent();
            dataIntent.setClass(HomeActivity.this, DataLoadingActivity.class);
            startActivity(dataIntent);
            HomeActivity.this.finish();
        }else{
            int position = bottomNavViewpager.getCurrentItem();
            for (int i = 0; i < bottomNav.getItemsCount(); i++) {
                if (bottomNav.getItem(i).getTitle(this).equals(targetFragmentTitle)) {
                    position = i;
                    break;
                }
            }
            if (position != -1) {
                bottomNavViewpager.setCurrentItem(position, true);
                bottomNav.setCurrentItem(position, true);
                //Refresh Data
                Fragment selectedFragment = ((FragmentPagerAdapter) bottomNavViewpager.getAdapter()).getItem(position);
                if (refreshFragmentData && selectedFragment.isAdded()) {
                    if (selectedFragment instanceof PlacesFragment) {
                        ((PlacesFragment) selectedFragment).fetchPlacesList();
                    } else if (selectedFragment instanceof NearByFragment) {
                        ((NearByFragment) selectedFragment).fetchLatLongList();
                    } else if (selectedFragment instanceof MyProfileFragment) {
                        ((MyProfileFragment) selectedFragment).fetchProfileData();
                    }
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
    }
