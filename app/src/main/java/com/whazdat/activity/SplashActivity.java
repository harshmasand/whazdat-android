package com.whazdat.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.whazdat.R;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;


/**
 * @author by Harsh Masand on 8/6/18.
 */

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ShimmerFrameLayout shimmerFrameLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        shimmerFrameLayout = findViewById(R.id.shimmer_container);
        shimmerFrameLayout.startShimmer();
        sharedPreferences = WhazdatSharedPrefs.getInstance();
        editor = WhazdatSharedPrefs.getEditor();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startNextActivity();

            }
        },3000);

    }

    private void startNextActivity() {
        if(sharedPreferences.getBoolean(SharedPrefsConstants.LOGGED_IN,false)){
            Intent dataLoadingIntent = new Intent(SplashActivity.this,DataLoadingActivity.class);
            startActivity(dataLoadingIntent);
        }else {
            startActivity(new Intent(SplashActivity.this,LoginActivity.class));
        }
        SplashActivity.this.finish();
    }
}

