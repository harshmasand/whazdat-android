package com.whazdat.activity;

import android.annotation.SuppressLint;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;
import com.whazdat.R;
import com.whazdat.constants.WhazdatIntentConstants;

/**
 * @author by Harsh Masand on 20/4/18.
 */

public class MediaViewActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener {

    private VideoView videoView;
    private MediaController mediaController;
    private ProgressBar progressBar;
    private String mediaURL;
    private MediaPlayer mediaPlayer;
    private TextView startTime,totalTime,toolbarTittle;
    private SeekBar audioProgress;
    private Handler threadHandler = new Handler();
    private FloatingActionButton playBtn;
    private UpdateSeekBarThread updateSeekBarThread;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_view);
        videoView = findViewById(R.id.video_view);
        progressBar = findViewById(R.id.loader);
        RelativeLayout audioContainer = findViewById(R.id.audio_container);
        RelativeLayout videoContainer = findViewById(R.id.video_container);
        startTime = findViewById(R.id.current_time);
        totalTime = findViewById(R.id.total_time);
        playBtn = findViewById(R.id.play_pause_fab);
        ImageView thumbnail = findViewById(R.id.thumbnail);
        ImageView forwardBtn = findViewById(R.id.forward);
        ImageView backwardBtn = findViewById(R.id.backward);
        audioProgress = findViewById(R.id.songProgressBar);
        mediaURL = getIntent().getStringExtra(WhazdatIntentConstants.MEDIA_URL);
        String imageURL = getIntent().getStringExtra(WhazdatIntentConstants.IMAGE_URL);
        String mediaType = getIntent().getStringExtra(WhazdatIntentConstants.MEDIA_TYPE);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbarTittle = findViewById(R.id.toolbar_title);
        if(mediaType.equalsIgnoreCase("audio")) {
            toolbar.setTitle("");
            toolbarTittle.setText("Audio");
            setupAudioPlayer();
            audioContainer.setVisibility(View.VISIBLE);
            videoContainer.setVisibility(View.GONE);
        }else{
            toolbar.setTitle("");
            toolbarTittle.setText("Video");
            setupVideoPlayer();
            videoContainer.setVisibility(View.VISIBLE);
            audioContainer.setVisibility(View.GONE);
        }
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Picasso.with(this)
                .load(imageURL)
                .into(thumbnail);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPauseAction();
            }
        });

        forwardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forwardAudio();
            }
        });

        backwardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rewindAudio();
            }
        });


    }

    private void setupAudioPlayer() {
        try{
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(mediaURL);
            mediaPlayer.prepare();
            mediaPlayer.setOnPreparedListener(MediaViewActivity.this);
            mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    progressBar.setVisibility(View.GONE);

                    return false;
                }
            });
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    playBtn.setImageResource(R.drawable.ic_play);
                }
            });
            updateSeekBarThread= new UpdateSeekBarThread();
            threadHandler.postDelayed(updateSeekBarThread,50);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void rewindAudio() {
        int currentPosition = mediaPlayer.getCurrentPosition();
        // 5 seconds.
        int SUBTRACT_TIME = 5000;

        if(currentPosition - SUBTRACT_TIME > 0 )  {
            mediaPlayer.seekTo(currentPosition - SUBTRACT_TIME);
        }
    }

    private void forwardAudio() {

        int currentPosition = mediaPlayer.getCurrentPosition();
        // 5 seconds.
        int ADD_TIME = 5000;
        if(currentPosition != mediaPlayer.getDuration()) {
            if (currentPosition + ADD_TIME > 0) {
                mediaPlayer.seekTo(currentPosition + ADD_TIME);
            }
        }
    }

    private void playPauseAction() {
        if(mediaPlayer != null) {
             if(mediaPlayer.isPlaying()){
                 playBtn.setImageResource(R.drawable.ic_play);
                 mediaPlayer.pause();
             }else{
                 playBtn.setImageResource(R.drawable.ic_pause);
                 mediaPlayer.start();
             }
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        progressBar.setVisibility(View.GONE);
        int duration = mediaPlayer.getDuration();
        int currentPosition = mediaPlayer.getCurrentPosition();
        if(currentPosition == 0){
            audioProgress.setMax(duration);
            totalTime.setText(String.valueOf(millisecondsToString(duration)));
        }else{
            mediaPlayer.reset();
        }
        mediaPlayer.start();
        //For showing progressbar loading if audio is buffering
        mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                switch (what) {
                    case MediaPlayer.MEDIA_INFO_BUFFERING_START: {
                        progressBar.setVisibility(View.VISIBLE);
                        return true;
                    }
                    case MediaPlayer.MEDIA_INFO_BUFFERING_END: {
                        progressBar.setVisibility(View.GONE);
                        return true;
                    }
                }
                return false;
            }
        });
    }

    class UpdateSeekBarThread implements Runnable{

        @Override
        public void run() {
            if(mediaPlayer != null) {
                int currentPosition = mediaPlayer.getCurrentPosition();
                String currentPositionStr = millisecondsToString(currentPosition);
                startTime.setText(currentPositionStr);
                audioProgress.setProgress(currentPosition);
                // Delay thread 50 millisecond.
                threadHandler.postDelayed(this, 50);
            }
        }
    }

    @SuppressLint("DefaultLocale")
    @SuppressWarnings("MalformedFormatString")
    private String millisecondsToString(int milliseconds)  {
        long minutes = ((milliseconds / 1000) / 60);
        long seconds =  ((milliseconds / 1000) % 60);
        return String.format("%02d",minutes)+":"+ String.format("%02d",seconds);
    }


    private void setupVideoPlayer() {
        if (mediaController == null) {
            mediaController = new MediaController(MediaViewActivity.this);
            // Set the videoView that acts as the anchor for the MediaController.
            mediaController.setAnchorView(videoView);
            // Set MediaController for VideoView
            videoView.setMediaController(mediaController);

        }
        if(mediaURL != null && !mediaURL.equalsIgnoreCase("")) {
            videoView.setVideoPath(mediaURL);
            videoView.requestFocus();
            videoView.start();
            // When the video file ready for playback.
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                public void onPrepared(MediaPlayer mediaPlayer) {
                    //For showing progressbar loading if video is buffering
                    mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                        @Override
                        public boolean onInfo(MediaPlayer mp, int what, int extra) {
                            switch (what) {
                                case MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START: {
                                    progressBar.setVisibility(View.GONE);
                                    return true;
                                }
                                case MediaPlayer.MEDIA_INFO_BUFFERING_START: {
                                    progressBar.setVisibility(View.VISIBLE);
                                    return true;
                                }
                                case MediaPlayer.MEDIA_INFO_BUFFERING_END: {
                                    progressBar.setVisibility(View.GONE);
                                    return true;
                                }
                            }
                            return false;
                        }
                    });
                }
            });
            videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    progressBar.setVisibility(View.GONE);
                    return false;
                }
            });
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // Store current position.
        savedInstanceState.putInt("CurrentPosition", videoView.getCurrentPosition());
        videoView.pause();
    }


    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Get saved position.
        int position = savedInstanceState.getInt("CurrentPosition");
        videoView.seekTo(position);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        threadHandler.removeCallbacks(updateSeekBarThread);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mediaPlayer != null) {
            mediaPlayer.pause();
        }
    }
}
