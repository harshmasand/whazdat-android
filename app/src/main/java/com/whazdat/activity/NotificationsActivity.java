package com.whazdat.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.whazdat.R;
import com.whazdat.util.ErrorView;

/**
 * @author by Harsh Masand on 1/4/18.
 */

public class NotificationsActivity extends AppCompatActivity {

    Toolbar toolbar;
    ErrorView errorView;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        toolbar = findViewById(R.id.toolbar);
        errorView = findViewById(R.id.error_view_container);
        recyclerView = findViewById(R.id.recycle_view_list);
        initToolbar();
    }

    private void initToolbar() {

            if (toolbar != null) {
                toolbar.setTitle(getString(R.string.notifications));
                setSupportActionBar(toolbar);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

    }
}
