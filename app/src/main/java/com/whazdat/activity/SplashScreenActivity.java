package com.whazdat.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.whazdat.R;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.service.FetchCurrentLocationIntentService;

/**
 * @author by Harsh Masand on 6/6/18.
 */

public class SplashScreenActivity extends AppCompatActivity implements FetchCurrentLocationIntentService.FetchCurrentLocationInterface {

    private static final int REQUEST_PERMISSIONS_LOCATION_SETTINGS_REQUEST_CODE = 33;
    private static final int REQUEST_PERMISSIONS_CURRENT_LOCATION_REQUEST_CODE = 35;
    FetchCurrentLocationIntentService fetchCurrentLocationIntentService;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    static SplashScreenActivity instance;
    public static SplashScreenActivity getInstance() {
        return instance;
    }

    static Context context;
    public static Context getNewApplicationContext() {
        return context;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = getApplicationContext();
        instance = this;
        fetchCurrentLocationIntentService = new FetchCurrentLocationIntentService();
        sharedPreferences = getSharedPreferences(getResources().getString(R.string.shared_preferences), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        Log.e("Splash","here");
        if(sharedPreferences.getBoolean(SharedPrefsConstants.LOGGED_IN,true)) {
            Intent loginIntent = new Intent(this, HomeActivity.class);
            startActivity(loginIntent);
        }else{
            Intent loginIntent = new Intent(this, LoginActivity.class);
            startActivity(loginIntent);
        }
    }

    @Override
    public void checkGPSSettings(SettingsClient settingsClient, LocationSettingsRequest.Builder builder) {
        settingsClient.checkLocationSettings(builder.build())
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        // Do Nothing I guess or call fetch Location
                        //TODO : check if this is working
                        fetchCurrentLocationIntentService.fetchLocation();
                        Log.e("GPS Success","here");

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        Log.e("GPS Failed","here");
                        switch (statusCode){
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(SplashScreenActivity.this, REQUEST_PERMISSIONS_LOCATION_SETTINGS_REQUEST_CODE);
                                } catch (IntentSender.SendIntentException sie) {
                                    sie.printStackTrace();
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getApplicationContext());
                                alertBuilder.setCancelable(false);
                                alertBuilder.setTitle(getString(R.string.not_enabled_location));
                                alertBuilder.setMessage(getString(R.string.enable_location_setting));
                                alertBuilder.setPositiveButton(getString(R.string.ok_text), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });
                                final AlertDialog dialog = alertBuilder.create();
                                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface arg0) {
                                        dialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.button_bg));
                                    }
                                });
                                dialog.show();                        }
                    }
                });
    }

    @Override
    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        Log.e("permission","here");
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    android.support.v7.app.AlertDialog.Builder alertBuilder = new android.support.v7.app.AlertDialog.Builder(getApplicationContext());
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Alert!");
                    alertBuilder.setMessage("Location permission is necessary for fetching your location");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSIONS_CURRENT_LOCATION_REQUEST_CODE);
                        }
                    });
                    android.support.v7.app.AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSIONS_CURRENT_LOCATION_REQUEST_CODE);
                }
                Log.e("should","false");
                return false;
            } else {
                return true;
            }
        }else{
            return true;
        }
    }

    @Override
    public void onLocationFound(Location location) {
        editor.putString(SharedPrefsConstants.LATITUDE, String.valueOf(location.getLatitude()));
        editor.putString(SharedPrefsConstants.LONGITUDE, String.valueOf(location.getLongitude())).apply();
        Toast.makeText(getApplicationContext(),"Location is " + location.getLatitude() + location.getLongitude(),Toast.LENGTH_SHORT).show();
        Intent loginIntent = new Intent(this,LoginActivity.class);
        startActivity(loginIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_PERMISSIONS_CURRENT_LOCATION_REQUEST_CODE:
                if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //fetch location and save the location
                    fetchCurrentLocationIntentService.createLocationRequestAndCheckLocationSettings(context);
                }
                break;
            default: super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }
        }
    }

