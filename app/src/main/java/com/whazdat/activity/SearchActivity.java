package com.whazdat.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.whazdat.R;
import com.whazdat.adapter.LocationSearchAdapter;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.ApiEndPointConstants;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.constants.WhazdatIntentConstants;
import com.whazdat.model.LocationSearchData;
import com.whazdat.model.LocationSearchResponse;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.InternetStatusHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * @author by Harsh Masand on 4/4/18.
 */

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = SearchActivity.class.getSimpleName();
    String searchTerm = "";
    AutoCompleteTextView searchTextView;
    Button searchNowBtn;
    Toolbar toolbar;
    TextView toolbarTitle;
    ImageView noResultsFound;
    ArrayList<LocationSearchData> searchDataList;
    InternetStatusHelper internetStatusHelper;
    SharedPreferences sharedPreferences;
    LocationSearchAdapter locationSearchAdapter;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        searchDataList = new ArrayList<>();
        internetStatusHelper = new InternetStatusHelper(this);
        sharedPreferences = WhazdatSharedPrefs.getInstance();
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        searchTextView = findViewById(R.id.location_search);
        searchNowBtn = findViewById(R.id.button_container);
        noResultsFound = findViewById(R.id.no_result_found_image);
        if (toolbar != null && toolbarTitle != null) {
            toolbar.setTitle("");
            toolbarTitle.setText("Search Location");
        }
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        searchTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //API Call
                searchTerm = s.toString();
                if(searchTerm.equalsIgnoreCase("")){
                    noResultsFound.setVisibility(View.GONE);
                    searchNowBtn.setVisibility(View.GONE);
                }else if(searchTerm.matches("-?\\d+(\\.\\d+)?")){
                    noResultsFound.setVisibility(View.GONE);
                    searchNowBtn.setVisibility(View.VISIBLE);
                }else{
                    searchNowBtn.setVisibility(View.GONE);
                    findLocation(searchTerm);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().equalsIgnoreCase("") || searchTextView.getText().toString().isEmpty()){
                    noResultsFound.setVisibility(View.GONE);
                }
                locationSearchAdapter.getFilter().filter(s.toString());

            }
        });
        searchNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openDetailedIntent = new Intent(SearchActivity.this,MyPlacesInnerActivity.class);
                openDetailedIntent.putExtra(WhazdatIntentConstants.ORDER_ID,Integer.valueOf(searchTerm));
                startActivity(openDetailedIntent);
                finish();
            }
        });
        locationSearchAdapter = new LocationSearchAdapter(SearchActivity.this,getApplicationContext(),R.layout.list_item_location,searchDataList);
        searchTextView.setAdapter(locationSearchAdapter);
        searchTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                searchTextView.setText("");
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                LocationSearchData searchData = locationSearchAdapter.getItem(position);
                Intent openDetailedIntent = new Intent(SearchActivity.this,MyPlacesInnerActivity.class);
                openDetailedIntent.putExtra(WhazdatIntentConstants.ORDER_ID,searchData.getOrderID());
                startActivity(openDetailedIntent);
                finish();
            }
        });

    }

    private void findLocation(final String term) {
        if(internetStatusHelper.isConnected()){
            Call<LocationSearchResponse> call = WhazdatApplication.getApiEndpointInterface().getSearchedLocation(ApiEndPointConstants.LOCATION_SEARCH,sharedPreferences.getInt(SharedPrefsConstants.ID,0),term);
            call.enqueue(new Callback<LocationSearchResponse>() {
                @Override
                public void onResponse(Call<LocationSearchResponse> call, Response<LocationSearchResponse> response) {
                    if(response.body() != null){
                        if(response.body().isSuccess()){
                            if(response.body().getData() != null) {
                                ArrayList<LocationSearchData> locationSearchData = response.body().getData();
                                if(locationSearchData != null && !locationSearchData.isEmpty()) {
                                    locationSearchAdapter.refreshAllList(locationSearchData);
                                    locationSearchAdapter.notifyDataSetChanged();
                                }
                            }
                        }else{
                            showNoDataView();
                        }
                    }else{
                        showNoDataView();
                    }
                }

                @Override
                public void onFailure(Call<LocationSearchResponse> call, Throwable t) {
                    if(t != null && t.getMessage() != null){
                        Log.e(TAG,t.getMessage()+"");
                    }
                    showNoDataView();
                }
            });
        }else{
            internetStatusHelper.showNoInternetToast(getApplicationContext());
        }
    }

    public void showNoDataView(){
        noResultsFound.setVisibility(View.VISIBLE);
    }

    public void hideNoDataView(){
        noResultsFound.setVisibility(View.GONE);
    }

}
