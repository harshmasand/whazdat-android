package com.whazdat.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.whazdat.R;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.ApiEndPointConstants;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.constants.WhazdatIntentConstants;
import com.whazdat.model.PlaceDetailsData;
import com.whazdat.model.PlaceDetailsResponse;
import com.whazdat.model.SetBookmarkResponse;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.ErrorView;
import com.whazdat.util.InternetStatusHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author by Harsh Masand on 19/4/18.
 */

public class MyPlacesInnerActivity extends AppCompatActivity implements OnMapReadyCallback{
    private static final String TAG = MyPlacesInnerActivity.class.getSimpleName();
    Toolbar toolbar;
    TextView toolbarTitle;
    String bookmark = "unpressed";
    MapView mMapView;
    GoogleMap mMap;
    InternetStatusHelper internetStatusHelper;
    ProgressBar loader;
    ScrollView dataContainer;
    ErrorView errorView;
    int orderID;
    SharedPreferences sharedPreferences;
    ImageView bookmarkIcon,placeImage,overLayIcon;
    RelativeLayout menuLayout, mediaContainer;
    TextView placeName,distance,dateOfBirth,placeDetails;
    String mediaType, mediaURL, imageURL;
    boolean showMenuIcon = false;
    boolean fromDataLoading;
    Double latitude,longitude;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_places_inner);
        internetStatusHelper = new InternetStatusHelper(this);
        sharedPreferences = WhazdatSharedPrefs.getInstance();
        orderID = getIntent().getIntExtra(WhazdatIntentConstants.ORDER_ID,0);
        fromDataLoading = getIntent().getBooleanExtra(WhazdatIntentConstants.FROM_DATA_LOADING,false);
        //Initiate Toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        loader = findViewById(R.id.loader);
        dataContainer = findViewById(R.id.data_container);
        mediaContainer = findViewById(R.id.media_container);
        overLayIcon = findViewById(R.id.overlay_icon);
        placeImage = findViewById(R.id.image_container);
        errorView = findViewById(R.id.error_view);
        mMapView = findViewById(R.id.map_view);
        placeName = findViewById(R.id.place_name);
        distance = findViewById(R.id.distance_value);
        dateOfBirth = findViewById(R.id.date_of_birth);
        placeDetails = findViewById(R.id.description_content);
        mMapView.onCreate(savedInstanceState);
        if (toolbar != null && toolbarTitle != null) {
            toolbar.setTitle("");
            toolbarTitle.setText(R.string.details);
        }
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        fetchPlaceDetails();

        mediaContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mediaType.equalsIgnoreCase("image")){
                    Intent mediaIntent = new Intent(MyPlacesInnerActivity.this, MediaViewActivity.class);
                    mediaIntent.putExtra(WhazdatIntentConstants.MEDIA_TYPE,mediaType);
                    mediaIntent.putExtra(WhazdatIntentConstants.MEDIA_URL,mediaURL);
                    mediaIntent.putExtra(WhazdatIntentConstants.IMAGE_URL,imageURL);
                    startActivity(mediaIntent);
                }
            }
        });

        errorView.setOnRetryButtonClickListener(new ErrorView.OnRetryButtonClickListener() {
            @Override
            public void onClick() {
                fetchPlaceDetails();
            }
        });
    }

    private void fetchPlaceDetails() {
        if(internetStatusHelper.isConnected()){
            showLoaderView();
            Call<PlaceDetailsResponse> call = WhazdatApplication.getApiEndpointInterface().getPlaceDetails(ApiEndPointConstants.PLACE_DETAILS,orderID,sharedPreferences.getInt(SharedPrefsConstants.ID,0),WhazdatApplication.getApplication().getLatitude(),WhazdatApplication.getApplication().getLongitude());
            call.enqueue(new Callback<PlaceDetailsResponse>() {
                @Override
                public void onResponse(Call<PlaceDetailsResponse> call, Response<PlaceDetailsResponse> response) {
                    if(response.body() != null){
                        PlaceDetailsResponse apiResponse = response.body();
                        if(apiResponse.isSuccess()){
                            PlaceDetailsData placeDetailsData = apiResponse.getData();
                            if(placeDetailsData != null) {
                                setPlaceDetailsValue(placeDetailsData);
                            }else{
                                showNoDataAvailableView();
                            }
                        }else{
                            showNoDataAvailableView();
                        }
                    }else{
                        //TODO Change it later once shukla changes code
                        showNoDataAvailableView();
                    }
                }

                @Override
                public void onFailure(Call<PlaceDetailsResponse> call, Throwable t) {
                    if(t != null && t.getMessage() != null){
                        Log.e(TAG,t.getMessage()+"");
                    }
                    showErrorView();
                }
            });
        }else{
            showNoInternetView();
        }
    }

    private void setPlaceDetailsValue(PlaceDetailsData placeDetailsData) {
        latitude = placeDetailsData.getLatitude();
        longitude = placeDetailsData.getLongitude();
        mMapView.getMapAsync(this);

        placeName.setText(placeDetailsData.getPlaceName());
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dob = simpleDateFormat.parse(placeDetailsData.getDob());
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateOfBirth.setText("BORN "+ newFormat.format(dob));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        distance.setText(placeDetailsData.getDistance()+ " AWAY");
        placeDetails.setText(placeDetailsData.getPlaceDescription());

        if(placeDetailsData.getPlaceImageURL() != null){
            imageURL = placeDetailsData.getPlaceImageURL();
            mediaType = "image";
            overLayIcon.setVisibility(View.GONE);
            Picasso.with(getApplicationContext())
                    .load(placeDetailsData.getPlaceImageURL())
                    .fit()
                    .centerCrop()
                    .into(placeImage);
        }

        if(placeDetailsData.getPlaceVideoURL() != null){
            mediaType = "video";
            mediaURL = placeDetailsData.getPlaceVideoURL();
            overLayIcon.setVisibility(View.VISIBLE);
        }else if(placeDetailsData.getPlaceAudioURL() != null){
            mediaType = "audio";
            mediaURL = placeDetailsData.getPlaceAudioURL();
            overLayIcon.setVisibility(View.VISIBLE);
        }

        showMenuIcon = true;
        if(placeDetailsData.isBookmark()) {
            bookmark = "unpressed";
        }else{
            bookmark = "pressed";
        }
        invalidateOptionsMenu();
        showDataView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bookmark, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        menuLayout = (RelativeLayout) menu.findItem(R.id.bookmark_item).getActionView();
        bookmarkIcon = menuLayout.findViewById(R.id.bookmark_icon);
        if(showMenuIcon){
            bookmarkIcon.setVisibility(View.VISIBLE);
        }else{
            bookmarkIcon.setVisibility(View.GONE);
        }
        if(bookmark.equalsIgnoreCase("unpressed")){
            bookmarkIcon.setImageResource(R.drawable.ic_bookmark_pressed);
            bookmark = "pressed";
        }else{
            bookmarkIcon.setImageResource(R.drawable.ic_bookmark_unpressed);
            bookmark = "unpressed";
        }
        menuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBookmark();
            }
        });

        return true;
    }

    private void setBookmark() {
        if(internetStatusHelper.isConnected()){
            final ProgressDialog pd = new ProgressDialog(this);
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.show();
            Call<SetBookmarkResponse> call = WhazdatApplication.getApiEndpointInterface().updateBookmarkStatus(ApiEndPointConstants.BOOKMARK,orderID,sharedPreferences.getInt(SharedPrefsConstants.ID,0),WhazdatApplication.getApplication().getLatitude(),WhazdatApplication.getApplication().getLatitude());
            call.enqueue(new Callback<SetBookmarkResponse>() {
                @Override
                public void onResponse(Call<SetBookmarkResponse> call, Response<SetBookmarkResponse> response) {
                    if(response.body() != null){
                        pd.dismiss();
                        SetBookmarkResponse apiResponse = response.body();
                        if(apiResponse.isSuccess()) {
                            if (apiResponse.getData() != null) {
                                if (apiResponse.getData().isBookmark()) {
                                    bookmark = "unpressed";
                                } else {
                                    bookmark = "pressed";
                                }
                                //Value set for Observers to Watch
                                apiResponse.getData().setBookmark(apiResponse.getData().isBookmark());
                                invalidateOptionsMenu();
                            } else {
                                Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        pd.dismiss();
                        Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SetBookmarkResponse> call, Throwable t) {
                    pd.dismiss();
                    if(t != null && t.getMessage() != null){
                        Log.e(TAG,t.getMessage()+"");
                    }
                    Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            internetStatusHelper.showNoInternetToast(getApplicationContext());
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setAllGesturesEnabled(false);
        LatLng latLng = new LatLng(latitude,longitude);
        mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.address_locations)).position(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));

    }

    public void showLoaderView(){
        loader.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
        dataContainer.setVisibility(View.GONE);
    }

    public void showDataView(){
        loader.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        dataContainer.setVisibility(View.VISIBLE);
    }

    public void showNoDataAvailableView(){
        showDataInfo(getString(R.string.no_results_text), getString(R.string.no_data_message),false);
    }

    public void showErrorView(){
        showDataInfo(getString(R.string.oops_text), getString(R.string.something_went_wrong_text),true);
    }

    public void showNoInternetView(){
        showDataInfo(getString(R.string.oops_text),getString(R.string.you_are_offline_text),true);
    }

    private void showDataInfo(String title, String subtitle,boolean showRetryButton) {
        showMenuIcon = false;
        invalidateOptionsMenu();
        errorView.changeTitleText(title);
        errorView.changeSubtitleText(subtitle);
        if (showRetryButton) {
            errorView.showButton();
        }else {
            errorView.hideButton();
        }
        loader.setVisibility(View.GONE);
        dataContainer.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    protected void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        try {
            mMapView.onLowMemory();
        } catch (Exception e) {
            Log.e(TAG, " " + e.getMessage(), e);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mMapView.onResume();
        } catch (Exception e) {
            Log.e(TAG, " " + e.getMessage(), e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            mMapView.onPause();

        } catch (Exception e) {
            Log.e(TAG, " " + e.getMessage(), e);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(fromDataLoading) {
            Intent homeIntent = new Intent(MyPlacesInnerActivity.this, HomeActivity.class);
            startActivity(homeIntent);
            MyPlacesInnerActivity.this.finish();
        }
    }
}
