package com.whazdat.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.whazdat.R;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.ApiEndPointConstants;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.model.LoginData;
import com.whazdat.model.LoginResponse;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.InternetStatusHelper;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author by Harsh Masand on 9/3/18.
 */

public class LoginActivity extends AppCompatActivity {

    private String TAG = LoginActivity.class.getSimpleName();
    EditText userNameEt, passwordEt, firstNameEt, lastNameEt, emailAddressEt, confirmPasswordEt;
    Button loginBtn;
    RelativeLayout userNameContainer, registerContainer, firstNameContainer, lastNameContainer, emailAddressContainer, confirmPasswordContainer, dobContainer;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    TextView registerBtn, dateOfBirth;
    InternetStatusHelper internetStatusHelper;
    boolean isRegistered = false;
    private int year;
    private int month;
    private int day;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        preferences = WhazdatSharedPrefs.getInstance();
        editor = WhazdatSharedPrefs.getEditor();
        internetStatusHelper = new InternetStatusHelper(getApplicationContext());
        userNameEt = findViewById(R.id.user_name_content);
        passwordEt = findViewById(R.id.password_content);
        firstNameEt = findViewById(R.id.first_name_content);
        lastNameEt = findViewById(R.id.last_name_content);
        emailAddressEt = findViewById(R.id.email_content);
        confirmPasswordEt = findViewById(R.id.confirm_password_content);
        userNameContainer = findViewById(R.id.user_name_container);
        registerContainer = findViewById(R.id.register_container);
        firstNameContainer = findViewById(R.id.first_name_container);
        lastNameContainer = findViewById(R.id.last_name_container);
        emailAddressContainer = findViewById(R.id.email_container);
        confirmPasswordContainer = findViewById(R.id.confirm_password_container);
        dobContainer = findViewById(R.id.date_of_birth_container);
        loginBtn = findViewById(R.id.button_container);
        registerBtn = findViewById(R.id.register_value);
        dateOfBirth = findViewById(R.id.date_of_birth_content);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginBtn.getText().toString().equalsIgnoreCase(getString(R.string.login))) {
                    if(validationForLogin()) {
                        login();
                    }
                } else {
                    if (checkAndContinue())
                        register();
                }
            }
        });
        dobContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDateDialog();
            }
        });
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRegisterView();
            }
        });
    }

    private void openDateDialog() {
        onCreateDialog();
    }

    private boolean validationForLogin(){
        boolean validationSuccessful = true;
        if(userNameEt.getText().toString().equalsIgnoreCase("") || userNameEt.getText().toString().isEmpty()){
            validationSuccessful = false;
            Toast.makeText(getApplicationContext(), "Please enter user name", Toast.LENGTH_SHORT).show();
        }else if(passwordEt.getText().toString().isEmpty() || passwordEt.getText().toString().equalsIgnoreCase("")){
            validationSuccessful = false;
            Toast.makeText(getApplicationContext(), "Please enter password", Toast.LENGTH_SHORT).show();
        }
        return validationSuccessful;
    }
    private boolean checkAndContinue() {
        
        boolean validationSuccessful = true;
        
        String userName = userNameEt.getText().toString();
        String firstName = firstNameEt.getText().toString();
        String lastName = lastNameEt.getText().toString();
        String dob = dateOfBirth.getText().toString();
        String emailAddress = emailAddressEt.getText().toString();
        String password = passwordEt.getText().toString();
        String confirmPassword = confirmPasswordEt.getText().toString();

        if(userName.isEmpty() || userName.equalsIgnoreCase("")){
            validationSuccessful = false;
            Toast.makeText(getApplicationContext(), "Please enter user name", Toast.LENGTH_SHORT).show();
        }else if(firstName.isEmpty() || firstName.equalsIgnoreCase("")){
            validationSuccessful = false;
            Toast.makeText(getApplicationContext(), "Please enter first name", Toast.LENGTH_SHORT).show();
        }else if(lastName.isEmpty() || lastName.equalsIgnoreCase("")){
            validationSuccessful = false;
            Toast.makeText(getApplicationContext(), "Please enter last name", Toast.LENGTH_SHORT).show();
        }else if(dob.isEmpty() || dob.equalsIgnoreCase("")) {
            validationSuccessful = false;
            Toast.makeText(getApplicationContext(), "Please enter last name", Toast.LENGTH_SHORT).show();
        }else if (!emailAddress.isEmpty() && !Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches()) {
            validationSuccessful = false;
            Toast.makeText(getApplicationContext(), "Please enter a valid email address", Toast.LENGTH_SHORT).show();
        } else if (!password.contentEquals(confirmPassword)) {
            validationSuccessful = false;
            Toast.makeText(getApplicationContext(), "Password does not match", Toast.LENGTH_SHORT).show();
        }
        return validationSuccessful; }


    @SuppressWarnings("deprecation")
    private void login() {
        if (internetStatusHelper.isConnected()) {
            final ProgressDialog pd = new ProgressDialog(this);
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.show();
            hideRegisterView();

            Call<LoginResponse> call = WhazdatApplication.getApiEndpointInterface().checkLogin(ApiEndPointConstants.LOGIN, userNameEt.getText().toString(), passwordEt.getText().toString());
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if (response.body() != null) {
                        LoginResponse apiResponse = response.body();
                        if (apiResponse.isSuccess()) {
                            LoginData loginData = apiResponse.getData();
                            if (loginData != null) {
                                pd.dismiss();
                                saveUserDetailsToSharedPrefs(loginData);
                                proceedAndContinueToNextActivity();
                            }
                        } else {
                            //User does not exists Show Register Content
                            pd.dismiss();
                            Toast.makeText(getApplicationContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        pd.dismiss();
                        Toast.makeText(getApplicationContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    pd.dismiss();
                    if (t != null && t.getMessage() != null) {
                        Log.e(TAG, t.getMessage() + "");
                    }
                    Toast.makeText(getApplicationContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            internetStatusHelper.showNoInternetToast(getApplicationContext());
        }
    }

    @SuppressWarnings("deprecation")
    private void register() {
        if (internetStatusHelper.isConnected()) {
            final ProgressDialog pd = new ProgressDialog(this);
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.show();
            Call<LoginResponse> call = WhazdatApplication.getApiEndpointInterface().registerUser(ApiEndPointConstants.REGISTER, userNameEt.getText().toString(),firstNameEt.getText().toString(),lastNameEt.getText().toString(),dateOfBirth.getText().toString(),emailAddressEt.getText().toString(), passwordEt.getText().toString());
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if (response.body() != null) {
                        LoginResponse apiResponse = response.body();
                        if (apiResponse.isSuccess()) {
                            LoginData loginData = apiResponse.getData();
                            if (loginData != null) {
                                pd.dismiss();
                                saveUserDetailsToSharedPrefs(loginData);
                                //show login view
                                if (!isRegistered) {
                                    isRegistered = true;
                                    login();
                                } else {
                                    Toast.makeText(getApplicationContext(), "User is already registered", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            pd.dismiss();
                            Toast.makeText(getApplicationContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        pd.dismiss();
                        Toast.makeText(getApplicationContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    pd.dismiss();
                    if (t != null && t.getMessage() != null) {
                        Log.e(TAG, t.getMessage() + "");
                    }
                    Toast.makeText(getApplicationContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            internetStatusHelper.showNoInternetToast(getApplicationContext());
        }
    }

    private void showRegisterView() {
        registerContainer.setVisibility(View.GONE);
        lastNameContainer.setVisibility(View.VISIBLE);
        firstNameContainer.setVisibility(View.VISIBLE);
        emailAddressContainer.setVisibility(View.VISIBLE);
        confirmPasswordContainer.setVisibility(View.VISIBLE);
        dobContainer.setVisibility(View.VISIBLE);
        isRegistered = false;
        loginBtn.setText(R.string.register);
    }

    private void hideRegisterView() {
        lastNameContainer.setVisibility(View.GONE);
        firstNameContainer.setVisibility(View.GONE);
        emailAddressContainer.setVisibility(View.GONE);
        confirmPasswordContainer.setVisibility(View.GONE);
        dobContainer.setVisibility(View.GONE);
        registerContainer.setVisibility(View.VISIBLE);
        loginBtn.setText(R.string.login);
    }

    private void saveUserDetailsToSharedPrefs(LoginData loginData) {
        editor.putBoolean(SharedPrefsConstants.LOGGED_IN, true);
        editor.putInt(SharedPrefsConstants.ID, loginData.getID());
        editor.putString(SharedPrefsConstants.USER_NAME, loginData.getDisplay_name());
        editor.putString(SharedPrefsConstants.USER_EMAIL_ADDRESS, loginData.getUser_email());
        editor.putString(SharedPrefsConstants.USER_IMAGE_URL, loginData.getUser_url()).apply();
    }

    private void proceedAndContinueToNextActivity() {
        Intent homeIntent = new Intent(this, DataLoadingActivity.class);
        startActivity(homeIntent);
        LoginActivity.this.finish();
    }

    protected Dialog onCreateDialog() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(LoginActivity.this, android.R.style.Theme_Holo_Light_Dialog, datePickerListener, year, month,
                        day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

        return dialog;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth + 1;
            day = selectedDay;
            @SuppressLint("DefaultLocale") String monthVal = String.format("%02d", month);
            @SuppressLint("DefaultLocale") String dayVal = String.format("%02d", day);
            StringBuilder dateStr = new StringBuilder().append(dayVal)
                        .append("-").append(monthVal).append("-").append(year);
                dateOfBirth.setText(dateStr);



        }
    };

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle(getString(R.string.alert));
        builder.setMessage(getString(R.string.sure_exit));
        builder.setCancelable(true);
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
