package com.whazdat.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.skyfishjy.library.RippleBackground;
import com.whazdat.R;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.ApiEndPointConstants;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.constants.WhazdatIntentConstants;
import com.whazdat.model.InitResponse;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.ErrorView;
import com.whazdat.util.InternetStatusHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author by Harsh Masand on 4/7/18.
 */
public class DataLoadingActivity extends AppCompatActivity {
    private static final String TAG = DataLoadingActivity.class.getSimpleName();


    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;


    private static final int REQUEST_CHECK_SETTINGS = 0x1;


    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    private final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    private final static String KEY_LOCATION = "location";
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private Boolean mRequestingLocationUpdates;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    RippleBackground rippleBackground;
    InternetStatusHelper internetStatusHelper;
    ErrorView errorView;
    boolean isInitCalled = false;






    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_loading);
        internetStatusHelper = new InternetStatusHelper(DataLoadingActivity.this);
        sharedPreferences = WhazdatSharedPrefs.getInstance();
        editor = WhazdatSharedPrefs.getEditor();
        rippleBackground= findViewById(R.id.content);
        rippleBackground.startRippleAnimation();
        mRequestingLocationUpdates = false;
        errorView = findViewById(R.id.error_view);
        // Update values using data stored in the Bundle.
        updateValuesFromBundle(savedInstanceState);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        // Kick off the process of building the LocationCallback, LocationRequest, and
        // LocationSettingsRequest objects.
        startUpdates();
        createLocationCallback();
        createLocationRequest();
        buildLocationSettingsRequest();
        errorView.setOnRetryButtonClickListener(new ErrorView.OnRetryButtonClickListener() {
            @Override
            public void onClick() {
                initCall();
            }
        });
    }

    private void startUpdates() {
        if (!mRequestingLocationUpdates) {
            mRequestingLocationUpdates = true;
            startLocationUpdates();
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    private void stopUpdates() {
        if (!mRequestingLocationUpdates) {
            Log.d(TAG, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }

        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mRequestingLocationUpdates = false;
                    }
                });
    }

    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                locationResult.getLocations().get(0);
                mCurrentLocation = locationResult.getLastLocation();
                saveLocationIntoSavedPrefs(mCurrentLocation);
            }
        };
    }

    private void saveLocationIntoSavedPrefs(Location mCurrentLocation) {
        if(mCurrentLocation != null){
            editor.putString(SharedPrefsConstants.LATITUDE, String.valueOf(mCurrentLocation.getLatitude()));
            editor.putString(SharedPrefsConstants.LONGITUDE, String.valueOf(mCurrentLocation.getLongitude())).apply();
            Log.e(TAG,"Latitude : " + mCurrentLocation.getLatitude());
            Log.e(TAG,"Longitude : " + mCurrentLocation.getLongitude());
            if(!isInitCalled) {
                initCall();
            }
        }
    }

    public void initCall(){
        if(internetStatusHelper.isConnected()){
            //Call<InitResponse> call = WhazdatApplication.getApiEndpointInterface().init(ApiEndPointConstants.INIT,Double.valueOf(sharedPreferences.getString(SharedPrefsConstants.LATITUDE,"")),Double.valueOf(sharedPreferences.getString(SharedPrefsConstants.LONGITUDE,"")));
            Call<InitResponse> call = WhazdatApplication.getApiEndpointInterface().init(ApiEndPointConstants.INIT,WhazdatApplication.getApplication().getLatitude(),WhazdatApplication.getApplication().getLongitude());
            call.enqueue(new Callback<InitResponse>() {
                @Override
                public void onResponse(Call<InitResponse> call, Response<InitResponse> response) {
                    if(response.body() != null){
                        isInitCalled = true;
                        if(response.body().isSuccess()){
                                Intent innerDataIntent = new Intent(DataLoadingActivity.this,MyPlacesInnerActivity.class);
                                innerDataIntent.putExtra(WhazdatIntentConstants.ORDER_ID,response.body().getData().getOrderID());
                                innerDataIntent.putExtra(WhazdatIntentConstants.FROM_DATA_LOADING,true);
                                startActivity(innerDataIntent);
                                DataLoadingActivity.this.finish();
                            }else{
                                proceedToNextActivity();
                            }
                        }else{
                            proceedToNextActivity();
                    }
                }

                @Override
                public void onFailure(Call<InitResponse> call, Throwable t) {
                    isInitCalled = true;
                    proceedToNextActivity();
                }
            });
        }else{
            showNoInternetView();
        }

    }

    private void proceedToNextActivity() {
        startActivity(new Intent(DataLoadingActivity.this, HomeActivity.class));
        DataLoadingActivity.this.finish();
    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }

            saveLocationIntoSavedPrefs(mCurrentLocation);
        }
    }

//    /**
//     * Stores activity data in the Bundle.
//     */
//    public void onSaveInstanceState(Bundle savedInstanceState) {
//        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
//        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
//        super.onSaveInstanceState(savedInstanceState);
//    }



    private void startLocationUpdates() {
        // Begin by checking if the device has the necessary location settings.
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");
                        Log.e("setting ok button","here");
                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                        saveLocationIntoSavedPrefs(mCurrentLocation);
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(DataLoadingActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);
                                Toast.makeText(DataLoadingActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                                mRequestingLocationUpdates = false;
                        }
                        saveLocationIntoSavedPrefs(mCurrentLocation);

                    }
                });
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions(){
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            //This will request permission
            ActivityCompat.requestPermissions(DataLoadingActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(DataLoadingActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mRequestingLocationUpdates = true;
                }
            }
        }

    @SuppressLint("MissingPermission")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        Log.e("GPS Setting ","here");
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        mRequestingLocationUpdates = false;
                        startLocationUpdates();

                        break;
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mRequestingLocationUpdates && checkPermissions()) {
            //Check for GPS Settings
            startLocationUpdates();
        } else  {
            requestPermissions();
        }
        saveLocationIntoSavedPrefs(mCurrentLocation);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopUpdates();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopUpdates();
    }

    public void showNoInternetView(){
        showDataInfo(getString(R.string.oops_text),getString(R.string.you_are_offline_text),true);
    }

    private void showDataInfo(String title, String subTitle,boolean showRetryButton) {
        errorView.changeTitleText(title);
        errorView.changeSubtitleText(subTitle);
        if (showRetryButton) {
            errorView.showButton();
        }else {
            errorView.hideButton();
        }
        rippleBackground.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }
}
