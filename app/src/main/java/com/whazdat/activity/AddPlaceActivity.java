package com.whazdat.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.badoualy.stepperindicator.StepperIndicator;
import com.whazdat.R;
import com.whazdat.adapter.PlaceSubscriptionAdapter;
import com.whazdat.application.WhazdatApplication;
import com.whazdat.constants.ApiEndPointConstants;
import com.whazdat.constants.SharedPrefsConstants;
import com.whazdat.fragments.AddPlaceDetailsFragment;
import com.whazdat.fragments.LatLngFragment;
import com.whazdat.fragments.MediaSelectionFragment;
import com.whazdat.fragments.PlaceSelectionDialogFragment;
import com.whazdat.model.AddPlacePackageResponse;
import com.whazdat.model.PlacePackageModel;
import com.whazdat.sharedprefs.WhazdatSharedPrefs;
import com.whazdat.util.InternetStatusHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPlaceActivity extends AppCompatActivity implements PlaceSelectionDialogFragment.SubscriptionSelectionListener {

    private static final String TAG = AddPlaceActivity.class.getSimpleName();
    LatLngFragment latLngFragment;
    AddPlaceDetailsFragment addPlaceDetailsFragment;
    MediaSelectionFragment mediaSelectionFragment;
    ViewPager viewPager;
    StepperIndicator stepperIndicator;
    int currentScreen;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    InternetStatusHelper internetStatusHelper;
    List<PlacePackageModel> placePackageModelList;
    ProgressBar loader;
    PlaceSubscriptionAdapter adapter;
    LinearLayout dataContainer;
    Button nextBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_place);
        nextBtn = findViewById(R.id.btn_cta_next);
        final Button backBtn = findViewById(R.id.btn_cta_back);
        loader = findViewById(R.id.loader);
        dataContainer = findViewById(R.id.data_container);
        internetStatusHelper = new InternetStatusHelper(getApplicationContext());
        sharedPreferences = WhazdatSharedPrefs.getInstance();
        editor = WhazdatSharedPrefs.getEditor();
        placePackageModelList = new ArrayList<>();
        viewPager = findViewById(R.id.view_pager);
        stepperIndicator = findViewById(R.id.step_indicator);
        adapter = new PlaceSubscriptionAdapter(getApplicationContext(), R.layout.list_item_subcription, placePackageModelList);
        SharedPreferences.Editor editor = WhazdatSharedPrefs.getEditor();
        editor.putString(SharedPrefsConstants.ADD_PLACE_LATITUDE,null);
        editor.putString(SharedPrefsConstants.ADD_PLACE_LONGITUDE,null).apply();
        Toolbar toolbar = findViewById(R.id.toolbar);

        if (toolbar != null) {
            toolbar.setTitle("Add Place");
            setSupportActionBar(toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            }            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

        }

        stepperIndicator.setCurrentStep(0);
        latLngFragment = new LatLngFragment();
        addPlaceDetailsFragment = new AddPlaceDetailsFragment();
        mediaSelectionFragment = new MediaSelectionFragment();


        final ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(latLngFragment);
        fragments.add(addPlaceDetailsFragment);
        fragments.add(mediaSelectionFragment);


        viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Do Nothing
            }

            @Override
            public void onPageSelected(int position) {
                currentScreen = position;
                stepperIndicator.setCurrentStep(position);
                switch (position){
                    case 0:
                        backBtn.setVisibility(View.GONE);
                        nextBtn.setText(R.string.next);
                        nextBtn.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        backBtn.setVisibility(View.VISIBLE);
                        nextBtn.setText(R.string.next);
                        nextBtn.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        backBtn.setVisibility(View.VISIBLE);
                        nextBtn.setText(R.string.finish);
                        nextBtn.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Do Nothing
            }
        });

        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(2);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (currentScreen){
                    case 0 : if(latLngFragment.isAdded()){
                        //API Call and
                        viewPager.setCurrentItem(currentScreen + 1, true);
                    }else{
                        recreate();
                    }
                        break;
                    case 1 : if(addPlaceDetailsFragment.isAdded()){
                        if(addPlaceDetailsFragment.validate()){
                            addPlaceDetailsFragment.addPlace();
                        } else {
                            Toast.makeText(getApplicationContext(), "Please add all the details", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        recreate();
                    }
                        break;

                    case 2:
                        if (mediaSelectionFragment.isAdded()) {
                            if (mediaSelectionFragment.validate()) {
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), "Please upload all the media files", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            recreate();
                        }
                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(currentScreen - 1, false);
            }
        });
        getPlaceDetails();
    }

    private void getPlaceDetails() {
        if (internetStatusHelper.isConnected()) {
            showLoader();
            Call<AddPlacePackageResponse> call = WhazdatApplication.getApiEndpointInterface().getPlaceDetails(ApiEndPointConstants.GET_USER_AUTH, sharedPreferences.getInt(SharedPrefsConstants.ID, 0));
            call.enqueue(new Callback<AddPlacePackageResponse>() {
                @Override
                public void onResponse(Call<AddPlacePackageResponse> call, Response<AddPlacePackageResponse> response) {
                    if (response.body() != null) {
                        if (response.body().isSuccess()) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                placePackageModelList = response.body().getData();
                                showSelectionDialog();
                            } else {
                                finish();
                                Toast.makeText(getApplicationContext(), "Please purchase the subscription from the website/adminpanel", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            finish();
                            Toast.makeText(getApplicationContext(), "Please purchase the subscription from the website/adminpanel", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        finish();
                        Toast.makeText(getApplicationContext(), "Please purchase the subscription from the website/adminpanel", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddPlacePackageResponse> call, Throwable t) {
                    Log.e(TAG, t.toString());
                    finish();
                    Toast.makeText(getApplicationContext(), "Please purchase the subscription from the website/adminpanel", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            internetStatusHelper.showNoInternetToast(getApplicationContext());
        }

    }

    private void showSelectionDialog() {
        FragmentManager manager = getSupportFragmentManager();
        PlaceSelectionDialogFragment alert = new PlaceSelectionDialogFragment(placePackageModelList, this);
        alert.show(manager, "show list");
    }

    @Override
    public void onPlaceSubscriptionSelection(PlacePackageModel placePackageModel) {
        editor.putString(SharedPrefsConstants.SECRET_HASH_KEY, placePackageModel.getSecretKeyHash());
        editor.putInt(SharedPrefsConstants.ORDER_ID, placePackageModel.getOrderID());
        editor.putInt(SharedPrefsConstants.AUDIO_SIZE, placePackageModel.getAudioSize());
        editor.putInt(SharedPrefsConstants.VIDEO_SIZE, placePackageModel.getVideoSize());
        editor.putInt(SharedPrefsConstants.IMAGE_SIZE, placePackageModel.getImageSize()).apply();
        showData();
    }

    public void showLoader() {
        loader.setVisibility(View.VISIBLE);
        dataContainer.setVisibility(View.GONE);
    }

    public void showData() {
        dataContainer.setVisibility(View.VISIBLE);
        loader.setVisibility(View.GONE);
    }

    public void nextScreen() {
        if (viewPager != null) {
            viewPager.setCurrentItem(currentScreen + 1, true);
        }

    }

    public void enableButton() {
        nextBtn.setEnabled(true);
    }


}
