package com.whazdat.util;

/**
 * @author Harsh Masand on 10/25/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;


public class InternetStatusHelper {

    Context context;
    public InternetStatusHelper(Context context) {
        this.context = context;

    }

    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public void showNoInternetToast(Context context){
        Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
    }
}
