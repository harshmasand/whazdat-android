package com.whazdat.util;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.whazdat.application.WhazdatApplication;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MediaManager {


    private Uri mCurrentImageFileUri = null;
    private Activity activity;
    private Fragment fragment;
    private int samplingFactor = 0;
    private int compressionQualityFactor = 100;
    public static final int SELECT_PICTURE = 12;
    public static final int SELECT_AUDIO = 13;



    public MediaManager() {
        //Empty constructor
    }

    public MediaManager(Activity activity) {
        this.activity = activity;
    }

    public MediaManager(Activity activity,Fragment fragment) {
        this.activity = activity;
        this.fragment = fragment;
    }



    /**
     * This function has to be compulsorily called from the overridden function onActivityResult in the
     * calling activity.
     *
     * @param requestCode Same as the requestCode parameter in onActivityResult method in calling activity
     * @param resultCode  Same as the resultCode parameter in onActivityResult method in calling activity
     * @param data        Same as the data parameter in onActivityResult method in calling activity
     * @return Uri Returns URI of the newly created image if resultCode is RESULT_OK else returns null
     * @throws Exception Thrown if there is some problem in FileOutputStream or if it cannot find the file or wrong request/result code
     */
    public Uri onActivityResult(int requestCode, int resultCode, Intent data, int orderID) throws Exception {
        if (requestCode == SELECT_PICTURE && resultCode == Activity.RESULT_OK) {
            storeCompressedImageFile(MediaStore.Images.Media.getBitmap(activity.getContentResolver(), data.getData()), getCompressionQualityFactor(), getSamplingFactor(), orderID);
            return getCurrentImageFileUri();
        } else{
            throw new Exception("Could not upload image\nPlease try again!");
        }

    }

    public void selectImageFromGallery() {

        Intent intent = new Intent(Intent.ACTION_PICK);
        // Show only images, no videos or anything else
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        if (fragment == null) {
            activity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
        } else {
            fragment.startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
        }

    }



    /**
     * This method compresses and stores the input bitmap in the form of a PNG image.
     *
     * @param sourceBitmap   Input bitmap
     * @param percentQuality Compression Factor(0-100).Higher the factor more the quality is preserved.
     * @throws IOException           Thrown if there is some issue in FileOutputStream
     * @throws FileNotFoundException Thrown if there is some issue in creation of image file.
     */
    private void storeCompressedImageFile(Bitmap sourceBitmap, int percentQuality, int sampleSize, int orderID) throws IOException {

        File imageFile = createImageFile(orderID);
        FileOutputStream fos = new FileOutputStream(imageFile);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        sourceBitmap.compress(Bitmap.CompressFormat.JPEG, percentQuality, stream);

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = sampleSize;
        bmOptions.inPurgeable = true;
        InputStream inputStream = new ByteArrayInputStream(stream.toByteArray());
        Log.e("image size ", (float) stream.toByteArray().length + " ");
        Bitmap compressedBitmap = BitmapFactory.decodeStream(inputStream, null, bmOptions);

        compressedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

        inputStream.close();

        stream.flush();
        stream.close();

        fos.flush();
        fos.close();

        setCurrentImageFileUri(imageFile);
    }



    //This method creates a new File which later holds an image.
    private File createImageFile(int userID) {
        File storageDir;
        if (WhazdatApplication.getExternalImageFilesDir() != null) {
            storageDir = WhazdatApplication.getExternalImageFilesDir();
        }else{
            storageDir = WhazdatApplication.getInternalImagesDir();
        }
        return new File(storageDir, userID + ".jpg");
    }

    private void setCurrentImageFileUri(File imageFile) {
        mCurrentImageFileUri = Uri.fromFile(imageFile);
    }

    private Uri getCurrentImageFileUri() {
        return mCurrentImageFileUri;
    }

    private int getCompressionQualityFactor() {
        return compressionQualityFactor;
    }

    private int getSamplingFactor() {
        return samplingFactor;
    }
}
