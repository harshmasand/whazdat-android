package com.whazdat.util;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.whazdat.R;

/**
 * @author by Harsh Masand on 12/3/18.
 */

public class ErrorView extends RelativeLayout {


    CardView errorLayoutContainer;
    TextView title,subTitle;
    Button button;
    OnRetryButtonClickListener onRetryButtonClickListener;
    ImageView errorImage;

    public ErrorView(Context context)
    {
        super(context);
        init();
    }

    public ErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ErrorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public interface  OnRetryButtonClickListener{
        void onClick();
    }

    public void setOnRetryButtonClickListener(OnRetryButtonClickListener onRetryButtonClickListener){
        this.onRetryButtonClickListener = onRetryButtonClickListener;
    }

    private void init() {
        inflate(getContext(), R.layout.error_view,this);
        errorLayoutContainer = findViewById(R.id.error_layout);
        title = findViewById(R.id.title_text);
        subTitle = findViewById(R.id.subtitle_text);
        button = findViewById(R.id.button);
        errorImage = findViewById(R.id.image);
        Picasso.with(getContext())
                .load(R.drawable.error_view)
                .placeholder(R.drawable.man)
                .fit()
                .into(errorImage);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onRetryButtonClickListener.onClick();
            }
        });
    }

    public void changeTitleText(String text){
        title.setText(String.valueOf(text));
    }

    public void changeButtonText(String text){
        title.setText(String.valueOf(text));
    }

    public void changeErrorImage(){
        //TODO: to be implement Later

    }

    public void changeTitleColor(String colorCode){
        int colorValue = Color.parseColor(colorCode);
        title.setTextColor(colorValue);

    }

    public void hideTitle(){
        title.setVisibility(GONE);
    }

    public void showTitle(){
        title.setVisibility(VISIBLE);
    }

    public void changeButtonColor(String colorCode){
        int colorValue = Color.parseColor(colorCode);
        button.setTextColor(colorValue);
    }

    public void changeButtonBackgroundColor(String colorCode){
        int colorValue = Color.parseColor(colorCode);
        button.setBackgroundColor(colorValue);
    }

    public void hideButton(){
        button.setVisibility(GONE);
    }

    public void showButton(){
        button.setVisibility(VISIBLE);
    }

    public void changeSubtitleText(String text){
        subTitle.setText(text);
    }

}


