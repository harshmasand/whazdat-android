package com.whazdat.sharedprefs;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.whazdat.constants.SharedPrefsConstants;


/**
 * @author by Harsh Masand on 8/6/18.
 */

public class WhazdatSharedPrefs {
    //Name for shared preference file
    private static final String SHARED_PREFS_NAME = WhazdatSharedPrefs.class.getSimpleName();
    private static final String TAG = WhazdatSharedPrefs.class.getSimpleName();
    private static SharedPreferences sharedPrefs;
    private static SharedPreferences.Editor editor;

    private WhazdatSharedPrefs(Context mAppContext) {
        sharedPrefs = mAppContext.getSharedPreferences(SHARED_PREFS_NAME, Activity.MODE_PRIVATE);
        editor = sharedPrefs.edit();
        if (!sharedPrefs.getBoolean(SharedPrefsConstants.SHARED_PREFS_CLEARED_ONCE, false)) {
            editor.clear();
            editor.commit();
            editor.putBoolean(SharedPrefsConstants.SHARED_PREFS_CLEARED_ONCE, true);
            editor.apply();
        }
    }

    //Initialise only once in application context
    public static void initialize(Context context) throws Exception {
        if (sharedPrefs != null) {
            throw new Exception("The SharedPreference instance is already initialized");
        }
        new WhazdatSharedPrefs(context);
    }

    //Create an instance of WhazdatSharedPrefs
    public static SharedPreferences getInstance() {
        if (sharedPrefs == null) {
            try {
                throw new Exception("Have You missed initializing the object?");
            } catch (Exception e) {
                Log.e(TAG, "" + e.getMessage(), e);
            }
        }
        return sharedPrefs;
    }

    //Create an instance of WhazdatSharedPrefs
    public static SharedPreferences.Editor getEditor() {
        if (editor == null) {
            try {
                throw new Exception("Have You missed initializing the object?");
            } catch (Exception e) {
                Log.e(TAG, "" + e.getMessage(), e);
            }
        }
        return editor;
    }
}
