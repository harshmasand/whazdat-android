package com.whazdat.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.whazdat.activity.SplashScreenActivity;

/**
 * @author by Harsh Masand on 6/6/18.
 */

public class FetchCurrentLocationIntentService extends IntentService {

    LocationRequest locationRequest;
    private static final long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private static final long FASTEST_INTERVAL = 2000; /* 2 sec */
    private static final int DISPLACEMENT = 10; // 10 meters


    Location currentLocation;
    FetchCurrentLocationInterface fetchCurrentLocationInterface;
    Context context;

    public FetchCurrentLocationIntentService() {
        super(FetchCurrentLocationIntentService.class.getSimpleName());
    }

    public interface FetchCurrentLocationInterface {

        boolean checkPermission();

        void checkGPSSettings(SettingsClient settingsClient, LocationSettingsRequest.Builder builder);

        void onLocationFound(Location location);
    }


    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public FetchCurrentLocationIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
         context = SplashScreenActivity.getNewApplicationContext();
         fetchCurrentLocationInterface = SplashScreenActivity.getInstance();
         Log.e("Service","here");
         if(fetchCurrentLocationInterface.checkPermission()) {
             createLocationRequestAndCheckLocationSettings(context);
         }
        }

    public void createLocationRequestAndCheckLocationSettings(Context context) {
        Log.e("Create Location Request","here");
        locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(DISPLACEMENT);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        SettingsClient settingsClient = LocationServices.getSettingsClient(context);
        //Check GPS Settings
        fetchCurrentLocationInterface.checkGPSSettings(settingsClient, builder);
        //fetch Location will be called via Check GPS Settings Success
        fetchLocation();
    }

    public void fetchLocation() {
        try {
            Log.e("fetchLocation","here");
            if(fetchCurrentLocationInterface != null) {
                if (fetchCurrentLocationInterface.checkPermission()) {
                    Log.e("getFused","here");
                    FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
                    fusedLocationProviderClient.requestLocationUpdates(locationRequest, new LocationCallback() {
                                @Override
                                public void onLocationResult(LocationResult locationResult) {
                                    Log.e("getFusedLocation","here");
                                    super.onLocationResult(locationResult);


                                    currentLocation = locationResult.getLastLocation();
                                    Log.e("Latitude :", String.valueOf(currentLocation.getLatitude()));
                                    Log.e("Longitude :", String.valueOf(currentLocation.getLongitude()));

                                    fetchCurrentLocationInterface.onLocationFound(currentLocation);

                                }

                        @Override
                        public void onLocationAvailability(LocationAvailability locationAvailability) {
                            super.onLocationAvailability(locationAvailability);
                            Log.e("Location Available","here");
                        }
                    },null);




                } else {
                    fetchCurrentLocationInterface.checkPermission();
                }
            }
        }catch (Exception e){
            Log.e("",e.getMessage()+"");
        }
    }
}
