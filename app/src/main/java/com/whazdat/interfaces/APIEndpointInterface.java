package com.whazdat.interfaces;

import com.whazdat.model.AddPlacePackageResponse;
import com.whazdat.model.BookmarkedPlacesResponse;
import com.whazdat.model.InitResponse;
import com.whazdat.model.LatLongListResponse;
import com.whazdat.model.LocationSearchResponse;
import com.whazdat.model.LoginResponse;
import com.whazdat.model.PlaceDetailsResponse;
import com.whazdat.model.PlaceListResponse;
import com.whazdat.model.SetBookmarkResponse;
import com.whazdat.model.StatusResponse;
import com.whazdat.model.SubscriptionResponse;
import com.whazdat.model.UserProfileResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author by Harsh Masand on 6/6/18.
 */

public interface APIEndpointInterface {

    @FormUrlEncoded
    @POST("/secure/bridge/api")
    Call<LoginResponse> checkLogin(@Field("type") String type,@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("/secure/bridge/api")
    Call<LoginResponse> registerUser(@Field("type") String type,@Field("username") String username,@Field("first_name") String firstName,@Field("last_name") String lastName,@Field("dob") String dob, @Field("email") String email,@Field("password") String password);

    @GET("/secure/bridge/api")
    Call<InitResponse> init(@Query("type") String type, @Query("lat") double latitude, @Query("lon") double longitude );


    @GET("/secure/bridge/api")
    Call<UserProfileResponse> showUserProfileData(@Query("type") String type, @Query("ci") int customerID);

    @GET("/secure/bridge/api")
    Call<SubscriptionResponse> getSubscriptionList(@Query("type") String type, @Query("ci") int customerID);

    @GET("/secure/bridge/api")
    Call<SetBookmarkResponse> updateBookmarkStatus(@Query("type") String type,@Query("oi") int orderID,@Query("ci") int customerID,@Query("lat") double latitude, @Query("lon") double longitude);

    @GET("/secure/bridge/api")
    Call<PlaceDetailsResponse> getPlaceDetails(@Query("type") String type, @Query("oi") int orderID, @Query("ci") int customerID, @Query("lat") double latitude, @Query("lon") double longitude);

    @GET("/secure/bridge/api")
    Call<PlaceListResponse> getPlaceList(@Query("type") String type, @Query("ci") int customerID, @Query("lat") double latitude, @Query("lon") double longitude);

    @GET("/secure/bridge/api")
    Call<LatLongListResponse> getLatLongList(@Query("type") String type, @Query("ci") int customerID, @Query("lat") double latitude, @Query("lon") double longitude);

    @GET("/secure/bridge/api")
    Call<LocationSearchResponse> getSearchedLocation(@Query("type") String type, @Query("ci") int customerID, @Query("term") String term);

    @GET("/secure/bridge/api")
    Call<BookmarkedPlacesResponse> getBookmarkedPlaces(@Query("type") String type,@Query("ci") int customerID);

    @GET("/secure/bridge/api")
    Call<AddPlacePackageResponse> getPlaceDetails(@Query("type") String type, @Query("ci") int customerID);

    @FormUrlEncoded
    @POST("/secure/addplace.php")
    Call<StatusResponse> addPlace(@Field("place_name") String placeName, @Field("place_lat") String latitude, @Field("place_lon") String longitude, @Field("place_about") String about, @Field("place_dob") String dob, @Field("place_dex") String dod, @Field("step") int step, @Field("oi") int orderID, @Field("key") String secretKey);

    @FormUrlEncoded
    @POST("/secure/addplace.php")
    Call<StatusResponse> addPlaceFinalAPI(@Field("step") int step, @Field("res_url") String url, @Field("oi") int orderID, @Field("key") String secretKey);
}
