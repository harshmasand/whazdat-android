package com.whazdat.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.whazdat.R;
import com.whazdat.model.PlacesData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author by Harsh Masand on 30/3/18.
 */

public class PlacesListAdapter extends RecyclerView.Adapter<PlacesListAdapter.MyViewHolder> {

    private Context context;
    private int resource;
    private List<PlacesData> myPlacesDataList;


    public PlacesListAdapter(Context context, int resource , List<PlacesData> myPlacesDataList) {
        super();
        this.context = context;
        this.resource = resource;
        this.myPlacesDataList = myPlacesDataList;
    }

    @NonNull
    @Override
    public PlacesListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(resource, parent, false);
        return new PlacesListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlacesListAdapter.MyViewHolder holder, int position) {
        PlacesData placesData = myPlacesDataList.get(position);
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = simpleDateFormat.parse(placesData.getDob());
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormatNew = new SimpleDateFormat("dd/MM/yyyy");
            holder.dateOfBirth.setText("BORN " + simpleDateFormatNew.format(date));
        }
        catch (ParseException e) {
                e.printStackTrace();
            }
        holder.distance.setText(placesData.getDistance() + " AWAY");
        holder.name.setText(placesData.getPlaceName());
        Picasso.with(context)
                .load(placesData.getThumbnail())
                .fit()
                .into(holder.thumbnail);

    }

    @Override
    public int getItemCount() {
        return myPlacesDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbnail;
        TextView name,distance,dateOfBirth;



        MyViewHolder(View itemView) {
            super(itemView);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            name = itemView.findViewById(R.id.name);
            distance = itemView.findViewById(R.id.distance_value);
            dateOfBirth = itemView.findViewById(R.id.date_of_birth);
        }
    }


}
