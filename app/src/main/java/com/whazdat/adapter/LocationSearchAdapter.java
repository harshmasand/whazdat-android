package com.whazdat.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.whazdat.R;
import com.whazdat.activity.SearchActivity;
import com.whazdat.model.LocationSearchData;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by Harsh Masand on 3/7/18.
 */
public class LocationSearchAdapter extends ArrayAdapter<LocationSearchData> implements Filterable {

    private Context context;
    private int resource;
    private ArrayList<LocationSearchData> originalList,filterableList;
    private SearchActivity activity;

    public LocationSearchAdapter(SearchActivity activity,Context context,int resource,ArrayList<LocationSearchData> locationSearchList){
        super(context,resource);
        this.context = context;
        this.activity = activity;
        this.resource = resource;
        this.originalList = new ArrayList<>(locationSearchList);
        filterableList = locationSearchList;
    }

    @SuppressWarnings("ConstantConditions")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(view  == null){
            if (inflater != null) {
                view = inflater.inflate(resource, parent, false);
                TextView placeName = view.findViewById(R.id.place_name);
                TextView orderID = view.findViewById(R.id.order_id_value);
                placeName.setText(filterableList.get(position).getPlaceName());
                orderID.setText("#"+filterableList.get(position).getOrderID());
            }
        }
        return view;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return mFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    private Filter mFilter = new Filter() {
        String filteredString;

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            List<LocationSearchData> list = originalList;
            int count = list.size();
            List<LocationSearchData> filteredList = new ArrayList<>();
            LocationSearchData filterableItem;
            if (filteredString.equalsIgnoreCase("") || filteredString.isEmpty()) {
                filteredList.addAll(originalList);
            } else {
                for (int i = 0; i < count; i++) {
                    filterableItem = list.get(i);
                    if ((filterableItem.getPlaceName() != null && !filterableItem.getPlaceName().trim().isEmpty() && filterableItem.getPlaceName().trim().toLowerCase().contains(filteredString))) {
                        filteredList.add(filterableItem);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values != null) {
                filterableList.clear();
                filterableList.addAll((ArrayList<LocationSearchData>) results.values);
                if(results.count != 0 ) {
                    if (filterableList.isEmpty() && !constraint.toString().matches("-?\\d+(\\.\\d+)?") && !constraint.toString().equalsIgnoreCase("")) {
                        activity.showNoDataView();
                    } else {
                        activity.hideNoDataView();
                    }
                }
                notifyDataSetChanged();
            }
        }
    };

    @Override
    public int getCount() {
        return filterableList.size();
    }

    @Override
    public  LocationSearchData getItem(int position) {
        return filterableList.get(position);
    }

    public void refreshAllList(List<LocationSearchData> newJobList) {
        if (originalList != null && filterableList!= null) {
            originalList.clear();
            originalList.addAll(newJobList);
            filterableList.clear();
            filterableList.addAll(newJobList);
        }

    }
}
