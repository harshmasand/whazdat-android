package com.whazdat.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.whazdat.R;
import com.whazdat.model.MyPlacesData;
import com.whazdat.model.MySubscriptionData;
import com.whazdat.model.PlacesData;

import java.util.List;

/**
 * @author by Harsh Masand on 16/3/18.
 */

public class MyPlacesListAdapter extends RecyclerView.Adapter<MyPlacesListAdapter.MyViewHolder> {

    private Context context;
    private int resource;
    private List<PlacesData> myPlacesDataList;


    public MyPlacesListAdapter(Context context, int resource ,List<PlacesData> myPlacesDataList) {
        super();
        this.context = context;
        this.resource = resource;
        this.myPlacesDataList = myPlacesDataList;

    }

    @NonNull
    @Override
    public MyPlacesListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(resource, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyPlacesListAdapter.MyViewHolder holder, int position) {
        holder.placeTitle.setText(myPlacesDataList.get(position).getPlaceName());
        Picasso.with(context)
                .load(myPlacesDataList.get(position).getThumbnail())
                .fit()
                .into(holder.placeImage);
    }

    @Override
    public int getItemCount() {
        return myPlacesDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView placeImage;
        TextView placeTitle;

        MyViewHolder(View itemView) {
            super(itemView);
            placeImage = itemView.findViewById(R.id.thumbnail);
            placeTitle = itemView.findViewById(R.id.title);
        }
    }
}
