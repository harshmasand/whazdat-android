package com.whazdat.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whazdat.R;
import com.whazdat.model.PlacePackageModel;

import java.util.List;

public class PlaceSubscriptionAdapter extends RecyclerView.Adapter<PlaceSubscriptionAdapter.MyViewHolder> {

    private Context context;
    private int resource;
    private List<PlacePackageModel> list;

    public PlaceSubscriptionAdapter(@NonNull Context context, int resource, List<PlacePackageModel> list) {
        super();
        this.context = context;
        this.resource = resource;
        this.list = list;
    }

    @Override
    public PlaceSubscriptionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(resource, parent, false);
        return new PlaceSubscriptionAdapter.MyViewHolder(itemView);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull PlaceSubscriptionAdapter.MyViewHolder holder, int position) {
        PlacePackageModel placeData = list.get(position);
        holder.subscriptionName.setText(placeData.getSubscriptionName());
        if (placeData.isChecked()) {
            holder.selected.setVisibility(View.VISIBLE);
        } else {
            holder.selected.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView subscriptionName;
        AppCompatImageView selected;


        MyViewHolder(View itemView) {
            super(itemView);
            subscriptionName = itemView.findViewById(R.id.subscription_name);
            selected = itemView.findViewById(R.id.ic_checked);
        }
    }
}
