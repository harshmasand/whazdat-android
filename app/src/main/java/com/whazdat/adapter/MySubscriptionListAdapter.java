package com.whazdat.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whazdat.R;
import com.whazdat.model.MySubscriptionData;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author by Harsh Masand on 16/3/18.
 */

public class MySubscriptionListAdapter extends RecyclerView.Adapter<MySubscriptionListAdapter.MyViewHolder> {

    private static final String TAG = MySubscriptionListAdapter.class.getSimpleName() ;
    private Context context;
    private List<MySubscriptionData> mySubscriptionDataList;
    private int resource;



    public MySubscriptionListAdapter(Context context, List<MySubscriptionData> mySubscriptionDataList, int resource) {
        super();
        this.context = context;
        this.resource = resource;
        this.mySubscriptionDataList = mySubscriptionDataList;
    }


    @Override
    public MySubscriptionListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(resource, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MySubscriptionListAdapter.MyViewHolder holder, int position) {
        MySubscriptionData mySubscriptionData = mySubscriptionDataList.get(position);
        holder.subscriptionName.setText(mySubscriptionData.getSubscriptionName());
        holder.imageSize.setText(String.valueOf(mySubscriptionData.getImageSize()) + " MB");
        holder.audioSize.setText(String.valueOf(mySubscriptionData.getAudioSize())  + " MB");
        holder.videoSize.setText(String.valueOf(mySubscriptionData.getVideoSize())  + " MB");
        try{
            @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = simpleDateFormat.parse(mySubscriptionData.getBoughtOn());
            @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormatNew = new SimpleDateFormat("dd/MM/yyyy");
            holder.subscriptionPurchasedDate.setText(simpleDateFormatNew.format(date));
        }catch (Exception e){
            Log.e(TAG,e.getMessage()+ "");
        }
        holder.orderID.setText("#"+String.valueOf(mySubscriptionData.getOrderID()));
    }

    @Override
    public int getItemCount() {
        return mySubscriptionDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView subscriptionName,imageSize,audioSize,videoSize,subscriptionPurchasedDate,orderID;


        MyViewHolder(View itemView) {
            super(itemView);
            subscriptionName = itemView.findViewById(R.id.subscription_name);
            imageSize = itemView.findViewById(R.id.image_size);
            audioSize = itemView.findViewById(R.id.audio_size);
            videoSize = itemView.findViewById(R.id.video_size);
            subscriptionPurchasedDate = itemView.findViewById(R.id.purchase_date);
            orderID = itemView.findViewById(R.id.order_id_value);

        }
    }
}
