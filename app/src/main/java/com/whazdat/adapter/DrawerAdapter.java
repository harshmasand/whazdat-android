package com.whazdat.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.whazdat.R;

import java.util.List;

public class DrawerAdapter extends ArrayAdapter<String> {
    private Context adapterContext;
    private List<String> navList;

    public DrawerAdapter(Context adapterContext, List<String> menuItems) {
        super(adapterContext, R.layout.list_item_side_nav);
        this.adapterContext = adapterContext;
        this.navList = menuItems;
    }

    @Override
    public int getCount() {
        return navList.size();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) adapterContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            v = inflater.inflate(R.layout.list_item_side_nav, parent, false);
        }

        TextView drawerText =  v.findViewById(R.id.drawer_text);

        drawerText.setText(navList.get(position));



        return v;
    }

}
