package com.whazdat.listener;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * @author by Harsh Masand on 28/4/18.
 */

public class RecyclerItemListener implements RecyclerView.OnItemTouchListener {

    private RecyclerTouchListener listener;
    private GestureDetector gd;

    public interface RecyclerTouchListener {
        void onClickItem(View v, int position) ;
        void onLongClickItem(View v, int position);
    }

    public RecyclerItemListener(Context context, final RecyclerView recyclerView,
                                final RecyclerTouchListener listener) {
        this.listener = listener;
        gd = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public void onLongPress(MotionEvent e) {
                        // We find the view
                        View v = recyclerView.findChildViewUnder(e.getX(), e.getY());
                        // Notify the even
                        listener.onLongClickItem(v, recyclerView.getChildAdapterPosition(v));
                    }

                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        View v = recyclerView.findChildViewUnder(e.getX(), e.getY());
                        // Notify the even
                        listener.onClickItem(v, recyclerView.getChildAdapterPosition(v));
                        return true;
                    }
                });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View child = rv.findChildViewUnder(e.getX(), e.getY());
        return ( child != null && gd.onTouchEvent(e));
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
